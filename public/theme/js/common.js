$(function(){
  'use strict';

  	// Master.blade js
  	// Menubar js
    $(window).resize(function(){
        minimizeMenu();
    });

    minimizeMenu();

    function minimizeMenu() {
        if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
        } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
        }
    }
	
	$('input[type=number]').on('mousewheel', function(e) {
		  $(e.target).blur();
	});
		
	$('#datatable').DataTable({
        pageLength: 50
    });

    $('#datatable1').DataTable();
    $('#datatable2').DataTable();
    
    $('input').attr('autocomplete', 'off');

    if($().select2) {
        $('#employee_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Employee"
        });

        $('#customer_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Customer"
        });
        
        $('#room_category_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Room Category"
        });

        $('#emp_department_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Department"
        });

        $('#emp_designation_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Designation"
        });

        $('#emp_company_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Company"
        });

        $('#emp_blood_group').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Blood Group"
        });

        $('#emp_gender_id').select2({
            minimumResultsForSearch: '',
            placeholder: "Select a Gender"
        });

        $('#userTypeId').select2({
            minimumResultsForSearch: '',
            placeholder: "Select Access Role"
        });

        $('#emp_department_id_edit').select2({
            minimumResultsForSearch: '',
            dropdownParent: $('#update_modal .modal-content'),
            placeholder: "Select a Department"
        });

        $('#emp_designation_id_edit').select2({
            minimumResultsForSearch: '',
            dropdownParent: $('#update_modal .modal-content'),
            placeholder: "Select a Designation"
        });

        $('#emp_company_id_edit').select2({
            minimumResultsForSearch: '',
            dropdownParent: $('#update_modal .modal-content'),
            placeholder: "Select a Company"
        });

        $('#emp_blood_group').select2({
            minimumResultsForSearch: '',
            dropdownParent: $('#update_modal .modal-content'),
            placeholder: "Select a Blood Group"
        });
        
        $('#expense_category_id').select2({
            minimumResultsForSearch: ''
        });

        $('#room_category_id_model').select2({
            minimumResultsForSearch: '',
            dropdownParent: $('#add_modal .modal-content'),
            placeholder: "Select a Category"
        });

        $("#branch_id").select2({
            placeholder: "Select Department"
        });
        
        $("#leave_type_id").select2({
            placeholder: "Select Employee First"
        });

    }

	    // date picker
	    $('.fc-datepicker').datepicker({
	        showOtherMonths: true,
	        selectOtherMonths: true,
	        dateFormat: 'YYYY-MM-DD'
	    }).on('changeDate', function(ev){
	        $(this).datepicker('hide');
	    });

		// date picker
	    $('.fc-datepicker1').datepicker({
	        showOtherMonths: true,
	        selectOtherMonths: true,
	        dateFormat: 'YYYY-MM-DD'
	    }).on('changeDate', function(ev){
	        $(this).datepicker('hide');
	    });


	    // date picker
	    $('.wh-datepicker').datepicker({
	        showOtherMonths: true,
	        selectOtherMonths: true,
	        dateFormat: 'YYYY-MM-DD'
	    }).on('changeDate', function(ev){
	        $(this).datepicker('hide');
	    });
	    
	    // date picker
	    $('.wh-datepicker1').datepicker({
	        showOtherMonths: true,
	        selectOtherMonths: true,
	        dateFormat: 'YYYY-MM-DD'
	    }).on('changeDate', function(ev){
	        $(this).datepicker('hide');
	    });

        // month picker
        $('.fc-monthpicker').datepicker({
                autoclose: true,
                minViewMode: 1,
                format: 'yyyy-mm'
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

	     // date picker
        $('.profile_update_picker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'YYYY-MM-DD'
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        // date picker
        $('.profile_update_picker1').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'YYYY-MM-DD'
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

     $("select.giveSoftAccessId").change(function () {
        var pin1= $(".giveSoftAccessId option:selected").val();

        if(pin1==1){
            $('#giveSoftAccessIdField1').show(500);
            $('#giveSoftAccessIdField2').show(500);
            $("#giveSoftAccessIdField1").prop('required',true);
            $("#giveSoftAccessIdField2").prop('required',true);
            $("#emp_email_id").prop('required',true);
            $("#requiredF1").css('color', 'red');
            $("#requiredF2").css('color', 'red');
            $("#requiredF3").css('color', 'red');


        }else{
            $('#giveSoftAccessIdField1').hide(500);
            $('#giveSoftAccessIdField2').hide(500);
            $("#emp_email_id").prop('required',false);
            $("#requiredF1").css('color', '#646c9a');
        }
    });

    $('#giveSoftAccessIdField1').hide();
    $('#giveSoftAccessIdField2').hide();

    function reload() {
        location.reload()
    }

});
