-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2020 at 11:17 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hrmaster`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(307, '2014_10_12_000000_create_users_table', 1),
(308, '2014_10_12_100000_create_password_resets_table', 1),
(309, '2019_08_19_000000_create_failed_jobs_table', 1),
(310, '2020_01_09_034113_create_company_information_table', 1),
(311, '2020_01_10_033923_create_product_category_table', 1),
(312, '2020_01_18_111855_create_product_sub_category_table', 1),
(313, '2020_01_20_060230_create_product_list_table', 1),
(314, '2020_01_20_061104_create_product_unit_list_table', 1),
(315, '2020_01_29_035326_create_employee_list_table', 1),
(316, '2020_01_29_040341_create_department_list_table', 1),
(317, '2020_01_29_040400_create_designation_list_table', 1),
(318, '2020_01_29_040427_create_gender_list_table', 1),
(319, '2020_01_29_040500_create_work_shift_list_table', 1),
(320, '2020_01_29_040705_create_employee_education_info_table', 1),
(321, '2020_01_29_040818_create_employee_others_info_table', 1),
(322, '2020_01_29_040833_create_employee_work_history_table', 1),
(323, '2020_01_29_040932_create_employee_nominee_table', 1),
(324, '2020_02_05_064903_create_fabric_library_table', 1),
(325, '2020_02_10_115413_create_fl_country_list_table', 1),
(326, '2020_02_10_115429_create_fl_mill_factory_list_table', 1),
(327, '2020_02_10_115444_create_fl_price_history_table', 1),
(328, '2020_02_13_100716_create_attendance_history_table', 1),
(329, '2020_02_13_111257_create_weekend_holiday_table', 1),
(330, '2020_02_15_034428_create_merchandising_bank_list_table', 1),
(331, '2020_02_15_041448_create_merchandising_buyer_list_table', 1),
(332, '2020_02_15_052411_create_merchandising_pi_list_table', 1),
(333, '2020_02_15_095052_create_pi_item_list_table', 1),
(334, '2020_02_17_044335_create_merchandising_factory_list_table', 1),
(335, '2020_02_17_052254_create_merchandising_commission_holder_list_table', 1),
(336, '2020_02_19_070843_create_merchan_factory_unit_list_table', 1),
(337, '2020_02_22_045110_create_merchan_season_phase_list_table', 1),
(338, '2020_02_22_060309_create_merchandising_po_list_table', 1),
(339, '2020_02_26_103808_create_merchan_product_department_table', 1),
(340, '2020_02_26_105805_create_merchan_development_progress_table', 1),
(341, '2020_02_29_053017_create_employee_leave_application_table', 1),
(342, '2020_02_29_091207_create_attendance_history_tmp_table', 1),
(343, '2020_03_02_125625_create_app_attendance_history_table', 1),
(344, '2020_03_02_171101_create_merchandising_pre_production_table', 1),
(345, '2020_03_03_153047_create_employee_shift_weekend_table', 1),
(346, '2020_03_03_175107_create_employee_leave_type_setting_table', 1),
(347, '2020_03_07_124155_create_holidays_observances_leave_table', 1),
(348, '2020_03_11_170541_create_jobs_table', 1),
(349, '2020_03_23_110652_create_merchandising_production_progress_table', 1),
(350, '2020_04_29_121028_create_sample_request_list_table', 1),
(351, '2020_05_03_131034_create_sample_development_history_table', 1),
(352, '2020_05_09_113824_create_fabric_sourcing_list_table', 1),
(353, '2020_05_10_100942_create_fabric_sourcing_mill_wise_report_tmp_table', 1),
(354, '2020_05_28_101003_create_expense_category_table', 2),
(355, '2020_05_28_101140_create_expense_list_table', 2),
(358, '2020_06_15_112632_create_employee_award_history_table', 3),
(359, '2020_06_15_124830_create_notice_list_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_attendance_history`
--

CREATE TABLE `tb_app_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_ref_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `check_in` time DEFAULT NULL,
  `check_out` time DEFAULT NULL,
  `check_in_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_app_attendance_history`
--

INSERT INTO `tb_app_attendance_history` (`id`, `attendance_ref_id`, `emp_id`, `attendance_date`, `check_in`, `check_out`, `check_in_lat`, `check_in_long`, `check_out_lat`, `check_out_long`, `check_in_ip`, `check_out_ip`, `created_at`, `updated_at`) VALUES
(1, 1, 1002, '2020-05-30', '16:32:05', '16:32:08', '23.869784499999998', '90.43047969999999', '23.869784499999998', '90.43047969999999', '::1', '::1', '2020-05-30 10:32:08', '2020-05-30 10:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history`
--

CREATE TABLE `tb_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `in_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_attendance_history`
--

INSERT INTO `tb_attendance_history` (`id`, `emp_id`, `attendance_date`, `in_time`, `out_time`, `attendance_type`, `created_at`, `updated_at`) VALUES
(1, 1002, '2020-05-30', '16:32:05', '16:32:08', 2, '2020-05-30 10:32:05', '2020-05-30 10:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history_tmp`
--

CREATE TABLE `tb_attendance_history_tmp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `punch_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_company_information`
--

CREATE TABLE `tb_company_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '1 for Active, 0 for Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_company_information`
--

INSERT INTO `tb_company_information` (`id`, `company_logo`, `company_name`, `company_tagline`, `company_phone`, `company_email`, `company_address1`, `company_address2`, `status`, `created_at`, `updated_at`) VALUES
(1, 'default.png', 'ABC Textile Limited.', 'Safe Textile Solution', '55087469', 'samsujjamanbappy@gmail.com', 'Sonargaon Janapath, Uttara 11, Dhaka 1230', NULL, 1, '2020-05-30 03:06:28', '2020-05-30 03:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_department_list`
--

CREATE TABLE `tb_department_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_department_list`
--

INSERT INTO `tb_department_list` (`id`, `department_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Production', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10', NULL),
(2, 'Merchandising', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10', NULL),
(3, 'Accounts & Finance', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10', NULL),
(4, 'Administration', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10', NULL),
(5, 'Sample', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10', NULL),
(6, 'Garment Technical & Quality', NULL, 1, 3, '2020-03-20 06:58:34', '2020-03-20 06:58:34', NULL),
(7, 'Fabric Sourcing & Technical', NULL, 1, 3, '2020-03-20 06:59:01', '2020-03-20 06:59:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_designation_list`
--

CREATE TABLE `tb_designation_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_designation_list`
--

INSERT INTO `tb_designation_list` (`id`, `designation_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Senior Merchandise Manager', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(2, 'Asst. Manager', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(3, 'Quality Controller', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(4, 'Sr. Merchandiser', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(5, 'Managing Director', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(6, 'Director', 'None', 1, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(7, 'Fabric Sourcing & Technical Manager', 'N/A', 1, 2, '2020-03-05 14:48:03', '2020-03-05 14:48:03'),
(8, 'Sample Man', NULL, 1, 3, '2020-03-17 08:00:18', '2020-03-17 08:00:18'),
(9, 'Production & planning Co-Ordinator', NULL, 1, 3, '2020-03-17 08:10:20', '2020-03-17 08:10:20'),
(11, 'Manager-Sample & Cad', NULL, 1, 3, '2020-03-17 08:12:23', '2020-03-17 08:12:23'),
(12, 'Office Assistant', NULL, 1, 3, '2020-03-17 08:13:01', '2020-03-17 08:13:01'),
(13, 'Sample Cutting Man', NULL, 1, 3, '2020-03-17 08:13:42', '2020-03-17 08:13:42'),
(14, 'Driver', NULL, 1, 3, '2020-03-17 08:14:09', '2020-03-17 08:14:09'),
(15, 'Managing Director', NULL, 1, 3, '2020-03-20 06:57:07', '2020-03-20 06:57:07'),
(16, 'Sr. Merchandiser - Research & Development', NULL, 1, 3, '2020-03-20 07:10:03', '2020-03-20 07:10:03'),
(17, 'Manager', NULL, 1, 3, '2020-03-20 07:52:56', '2020-03-20 07:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_award_history`
--

CREATE TABLE `tb_employee_award_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) DEFAULT NULL COMMENT 'employee_id will come from ("tb_employee_list") table',
  `award_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_gift` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_price` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_period` date DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_education_info`
--

CREATE TABLE `tb_employee_education_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `emp_exam_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_institution_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_result` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_scale` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_passing_year` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_application`
--

CREATE TABLE `tb_employee_leave_application` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `leave_type_id` bigint(20) DEFAULT NULL,
  `leave_starting_date` date DEFAULT NULL,
  `leave_ending_date` date DEFAULT NULL,
  `actual_days` int(11) DEFAULT NULL,
  `approved_by` tinyint(4) DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_leave_application`
--

INSERT INTO `tb_employee_leave_application` (`id`, `unique_id`, `employee_id`, `leave_type_id`, `leave_starting_date`, `leave_ending_date`, `actual_days`, `approved_by`, `attachment`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 2, 1, '2020-05-22', '2020-05-25', 4, 2, NULL, NULL, 1, '2', '2020-05-30 10:29:32', '2020-05-30 10:29:52'),
(2, NULL, 2, 1, '2020-05-23', '2020-05-25', 3, 2, NULL, NULL, 2, '2', '2020-05-30 10:29:32', '2020-05-30 10:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_type_setting`
--

CREATE TABLE `tb_employee_leave_type_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `leave_type_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_leave_days` int(11) DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_leave_type_setting`
--

INSERT INTO `tb_employee_leave_type_setting` (`id`, `company_id`, `leave_type_name`, `total_leave_days`, `remarks`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sick Leave', 10, 'None', 2, 1, '2020-05-30 09:40:01', '2020-05-30 09:40:01'),
(2, 1, 'Casual Leave', 10, NULL, 2, 1, '2020-06-15 08:57:30', '2020-06-15 08:57:30'),
(3, 1, 'Earn Leave', 10, NULL, 2, 1, '2020-06-15 08:58:57', '2020-06-15 08:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_list`
--

CREATE TABLE `tb_employee_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_first_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_department_id` int(11) DEFAULT NULL,
  `emp_designation_id` int(11) DEFAULT NULL,
  `emp_gender_id` int(11) DEFAULT NULL,
  `emp_shift_id` int(11) DEFAULT NULL,
  `emp_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_dob` date DEFAULT NULL,
  `emp_joining_date` date DEFAULT NULL,
  `emp_probation_period` int(11) DEFAULT NULL,
  `emp_religion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_marital_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_account` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_card_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_discontinuation` date DEFAULT NULL,
  `reason_of_discontinuation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nationality` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_parmanent_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_current_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_father_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_mother_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_ipbx_extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_account_status` tinyint(4) DEFAULT NULL,
  `created_by` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_list`
--

INSERT INTO `tb_employee_list` (`id`, `employee_id`, `company_id`, `emp_first_name`, `emp_last_name`, `emp_department_id`, `emp_designation_id`, `emp_gender_id`, `emp_shift_id`, `emp_email`, `emp_phone`, `emp_photo`, `emp_dob`, `emp_joining_date`, `emp_probation_period`, `emp_religion`, `emp_marital_status`, `emp_bank_account`, `emp_bank_info`, `emp_card_number`, `emp_blood_group`, `date_of_discontinuation`, `reason_of_discontinuation`, `emp_nid`, `emp_nationality`, `emp_parmanent_address`, `emp_current_address`, `emp_father_name`, `emp_mother_name`, `emp_ipbx_extension`, `emp_account_status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1001', '1', 'Samsujjaman ', 'Bappy', 3, 2, 1, 1, 'aziz@dongyi-bd.com', '01918-792614', '1581335076.png', '1988-08-05', '2020-01-15', NULL, 'Islam', 'Married', '1053204000002878', 'UCBL - Sonargaon Janapath Branch', NULL, 'B+', NULL, NULL, '2385937061', 'Bangladeshi', 'Vill: Rumchandrapur, Post: Rajfulbaria, P.S: Savar, Dist: Dhaka.', 'Vill: Rumchandrapur, Post: Rajfulbaria, P.S: Savar, Dist: Dhaka.', 'Md. Razzab Ali', 'Anowara Begum', '123', 1, '3', '2020-02-07 10:59:32', '2020-03-20 07:29:15'),
(2, '1002', '1', 'Mr. Abdullah', 'Haque', 2, 4, 1, 1, 'nazmul@dongyi-bd.com', '01747424874', '1581335119.png', '1995-10-10', '2020-01-15', NULL, 'Islam', 'Married', NULL, '1053204000002823', NULL, 'O+', NULL, NULL, '1026117109', 'Bangladeshi', 'Jangon, Muradnagor, Comilla', 'S # 05, R # 05, H # 58 (Left Side), Uttara', 'Moslah Uddin', 'Mazeda Begum', '112', 1, '2', '2020-02-10 15:49:44', '2020-02-11 23:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_nominee`
--

CREATE TABLE `tb_employee_nominee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `nominee_name` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_relation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_others_info`
--

CREATE TABLE `tb_employee_others_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_shift_weekend`
--

CREATE TABLE `tb_employee_shift_weekend` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_id` tinyint(4) DEFAULT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '"1" is weekend or  "0" Office Day',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_shift_weekend`
--

INSERT INTO `tb_employee_shift_weekend` (`id`, `shift_id`, `day_name`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 1, 'Saturday', 1, 2, '2020-06-15 09:10:58', '2020-06-15 09:10:58'),
(4, 1, 'Friday', 1, 2, '2020-06-15 09:10:58', '2020-06-15 09:10:58');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_work_history`
--

CREATE TABLE `tb_employee_work_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `wh_company_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_designation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_joining_date` date DEFAULT NULL,
  `wh_resign_date` date DEFAULT NULL,
  `wh_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_expense_category`
--

CREATE TABLE `tb_expense_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_expense_category`
--

INSERT INTO `tb_expense_category` (`id`, `category_name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Food', NULL, 1, 2, '2020-06-15 09:08:18', '2020-06-15 09:08:18'),
(2, 'Electricity', NULL, 1, 2, '2020-06-15 09:08:45', '2020-06-15 09:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_expense_list`
--

CREATE TABLE `tb_expense_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) DEFAULT NULL COMMENT 'category_id come from ("tb_expense_category") table',
  `amount` double(18,2) DEFAULT NULL,
  `expense_date` date DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `approved_by` bigint(20) DEFAULT NULL COMMENT 'approved By',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gender_list`
--

CREATE TABLE `tb_gender_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gender_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_gender_list`
--

INSERT INTO `tb_gender_list` (`id`, `gender_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Male', 'None', 1, NULL, '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(2, 'Female', 'None', 1, NULL, '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(3, 'Other', 'None', 1, NULL, '2020-05-30 03:06:28', '2020-05-30 03:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_holidays_observances_leave`
--

CREATE TABLE `tb_holidays_observances_leave` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `holiday_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_holidays_observances_leave`
--

INSERT INTO `tb_holidays_observances_leave` (`id`, `company_id`, `holiday_title`, `start_date`, `end_date`, `remarks`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'International Mother Language Day', '2020-03-21', '2020-03-21', 'None', 2, 1, '2020-06-15 09:02:28', '2020-06-15 09:04:45'),
(2, 1, 'Sheikh Mujibur Rahman\'s birthday', '2020-03-17', '2020-03-17', NULL, 2, 1, '2020-06-15 09:04:24', '2020-06-15 09:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notice_list`
--

CREATE TABLE `tb_notice_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notice_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_notice_list`
--

INSERT INTO `tb_notice_list` (`id`, `notice_title`, `start_date`, `end_date`, `description`, `attachment`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'TEST', '2020-06-18', '2020-07-11', 'None', 'C:\\xampp\\tmp\\php1205.tmp', 2, 1, '2020-06-15 07:11:45', '2020-06-15 07:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_weekend_holiday`
--

CREATE TABLE `tb_weekend_holiday` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `company_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_weekend_holiday`
--

INSERT INTO `tb_weekend_holiday` (`id`, `day_name`, `status`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', 1, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(2, 'Sunday', 0, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(3, 'Monday', 0, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(4, 'Tuesday', 0, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(5, 'Wednesday', 0, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(6, 'Thursday', 0, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28'),
(7, 'Friday', 1, '1', '2020-05-30 03:06:28', '2020-05-30 03:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `tb_work_shift_list`
--

CREATE TABLE `tb_work_shift_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exit_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buffer_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_work_shift_list`
--

INSERT INTO `tb_work_shift_list` (`id`, `shift_name`, `entry_time`, `exit_time`, `buffer_time`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Default', '09:00', '18:00', '09:15', 'None', 1, 2, '2020-05-30 09:45:36', '2020-05-30 09:45:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_ip` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 for inactive user and 1 for active user',
  `last_login_at` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_id`, `user_type`, `company_id`, `name`, `email`, `password`, `login_ip`, `status`, `last_login_at`, `created_by`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 'super admin', 'super@email.com', '$2y$10$MSG74t9sdceazVNL7pN8u.v1HqVK60aVzIx2qCFfL0J5Czpou/pge', NULL, 1, '2020-02-06 10:52:10', 1, NULL, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(2, '1', 2, 1, 'admin', 'admin@email.com', '$2y$10$B20H.4ikm2hlbS946.LrO.7Rm0OZSz3PzU7uiGBtQ.07IDd.Af8s2', NULL, 1, '2020-02-06 10:52:10', 1, NULL, NULL, '2020-02-07 10:52:10', '2020-02-07 10:52:10'),
(3, '2', 12, 1, 'Employee', 'employee@email.com', '$2y$10$Ya/CBYwHgQu9InwYF0BcfOf9qevUHxoRyYuZ/a6B1Gx5UTNj.dGAa', NULL, 1, NULL, 1, NULL, 'dyPeVmYPFR0xEdUTBs1cJJzengUFhzBovnSg3XZ63bXg4ArGMLq5shnQC1Ds', '2020-02-07 10:59:32', '2020-06-15 08:48:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_app_attendance_history_attendance_ref_id_index` (`attendance_ref_id`),
  ADD KEY `tb_app_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_tmp_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_award_history`
--
ALTER TABLE `tb_employee_award_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_education_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_leave_application_employee_id_index` (`employee_id`),
  ADD KEY `tb_employee_leave_application_leave_type_id_index` (`leave_type_id`);

--
-- Indexes for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_list_company_id_index` (`company_id`),
  ADD KEY `tb_employee_list_emp_department_id_index` (`emp_department_id`),
  ADD KEY `tb_employee_list_emp_designation_id_index` (`emp_designation_id`),
  ADD KEY `tb_employee_list_emp_shift_id_index` (`emp_shift_id`);

--
-- Indexes for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_nominee_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_others_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_work_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_expense_category`
--
ALTER TABLE `tb_expense_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_expense_list`
--
ALTER TABLE `tb_expense_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_gender_list_gender_name_unique` (`gender_name`);

--
-- Indexes for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notice_list`
--
ALTER TABLE `tb_notice_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_weekend_holiday_company_id_index` (`company_id`);

--
-- Indexes for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=360;

--
-- AUTO_INCREMENT for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_employee_award_history`
--
ALTER TABLE `tb_employee_award_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_expense_category`
--
ALTER TABLE `tb_expense_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_expense_list`
--
ALTER TABLE `tb_expense_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_notice_list`
--
ALTER TABLE `tb_notice_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
