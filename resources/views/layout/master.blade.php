<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="hrMaster:MSBCSE">
    <meta name="author" content="hrMaster:MSBCSE">
    <link rel='shortcut icon' type='image/x-icon' href="{{asset('theme/img/favicon.png')}}">
    <title>
        @yield('title')
    </title>
    <!-- vendor css -->
    @include('include.css')
    @yield('extra_css')
</head>
<body >

<!-- ########## START: LEFT PANEL ########## -->
@include('include.logo')
<!-- br-sideleft -->
@include('include.left_sidebar')
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<!-- br-header -->
@include('include.header_top')
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: RIGHT PANEL ########## -->
<!-- br-sideright -->
{{-- @include('include.right_sidebar') --}}
<!-- ########## END: RIGHT PANEL ########## --->

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    @yield('content')
    @include('include.footer')
</div>
@include('include.js')
@yield('extra_js')
</body>
</html>
