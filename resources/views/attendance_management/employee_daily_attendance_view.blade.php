@extends('layout.master')
@section('title','Date Wise Attendance History')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Date Wise Attendance History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">

               {!! Form::open(['method'=>'POST','route'=>'employee.attendance.daily_attendance']) !!}
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Date <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control fc-datepicker" placeholder="Click here to select date" name="attendance_date" autocomplete="off" required="" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                             <button class="btn btn-lg btn-teal" type="submit">View</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection
@section('extra_js')
@endsection