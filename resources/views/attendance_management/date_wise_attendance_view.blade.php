@extends('layout.master')
@section('title','Date Wise Attendance History')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Date Wise Attendance History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                {!! Form::open(['method'=>'POST','route'=>'employee.attendance.date_wise_attendance_data']) !!}
                <div class="row">
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Attendance From <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control fc-datepicker" placeholder="Click here to select date" name="search_from" autocomplete="off" required="" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Attendance To <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control fc-datepicker" placeholder="Click here to select date" name="search_to" autocomplete="off" required="" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12  col-12">
                                <div class="form-group row select_2_row_modal">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Employee <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control" name="employee_id" id="employee_id" required=""  data-placeholder="Employee">
                                            <option selected="" value="all">All Employee</option>
                                            @foreach($employee_list as $employee)
                                                <option value="{{$employee->id}}">{{$employee->emp_first_name}} {{$employee->emp_last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                          <div class="col-sm-12"><br><br></div>

                          <div class="form-group text-center col-sm-12">
                              <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="btnSubmit" type="submit"><i class="fa fa-save"></i>  View History</button>
                              <button type="reset" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"> <i class="fa fa-times-circle"></i> Reset</button>
                          </div>
                        </div>
                    </div>
                </div>
               {!! Form::close() !!}
            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection
@section('extra_js')
@endsection