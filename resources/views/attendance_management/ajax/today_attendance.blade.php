
@section('extra_js')
    <script>
        $(document).ready(function(){

            //Employee attendance list start
            $('#employee_attendance_list').DataTable({
                processing: true,
                serverSide: true,
                "pageLength": 25,
                ajax:{
                    url: "{{ route('employee.attendance.todays_attendance') }}",
                    data:{
                        "search_employee": "{{Request::get('search_employee')}}",
                        "search_from": "{{Request::get('search_from')}}",
                        "search_to": "{{Request::get('search_to')}}"
                    }
                },
                columns:[
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'entry_date',
                        name: 'entry_date',
                    },
                    {
                        data: 'day',
                        render: function(data, type, row, meta){
                            if (row.status == 'Weekend') {
                                return '<span class="msb-txt-blue">'+data+'</span>'
                            }else{
                                return data
                            }
                        }
                    },
                    {
                        data: 'employee_id',
                        name: 'employee_id'
                    },
                    {
                        data: 'employee_name',
                        name: 'employee_name'
                    },
                    {
                        data: 'shift',
                        name: 'shift'
                    },
                    {
                        data: 'start_time',
                        name: 'start_time'
                    },
                    {
                        data: 'in_time',
                        name: 'in_time'
                    },
                    {
                        data: 'late',
                        render: function(data, type, row, meta){
                            if (Boolean(data)) {
                                return '<span class="msb-txt-red">'+data+'</span>'
                            }else{
                                return data
                            }
                        }
                    },
                    {
                        data: 'out_time',
                        render: function(data, type, row, meta){
                            return "{{ date('h:i a', strtotime("+data+")) }}"
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                        render: function(data, type, full, meta){
                            if (data == 'Absent') {
                                return '<span class="msb-bg-red msb-txt-white msb-pd-3">'+data+'</span>'
                            }
                            
                            if (data == 'Late') {
                                return '<span class="msb-bg-late msb-txt-white msb-pd-3">'+data+'</span>'
                            }

                            if (data == 'Present-Weekend') {
                                return '<span class="msb-bg-green msb-txt-white msb-pd-3">'+data+'</span>'
                            }

                            if (data == 'Weekend') {
                                return '<span class="msb-bg-weekend msb-txt-white msb-pd-3">'+data+'</span>'
                            }

                            if (data == 'Present') {
                                return '<span class="msb-bg-green msb-txt-white msb-pd-3">'+data+'</span>'
                            }

                            if (data == 'Leave') {
                                return '<span class="msb-bg-leave msb-txt-white msb-pd-3">'+data+'</span>'
                            }
                        }
                    }
                ]
            });
            //Employee attendance list end

            if($().select2) {
                $('.select2').select2({
                    dropdownParent: $('#add_search_modal'),
                    placeholder: "--Select One--",
                    allowClear:true
                });
            }

            // date picker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd-mm-yy',
                autoclose: true
            });

        });
    </script>
@endsection



