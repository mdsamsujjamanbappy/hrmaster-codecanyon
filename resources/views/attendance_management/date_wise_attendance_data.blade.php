@extends('layout.master')
@section('title','Date Wise Attendance Report')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Date Wise Attendance Report (From: {{date('d-m-Y', strtotime($request->search_from))}} to {{date('d-m-Y', strtotime($request->search_to))}})</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <table id="datatable" class="table table-bordered display responsive">
                    <thead>
                        <tr>
                            <th class="msb-txt-center">SL</th>
                            <th class="msb-txt-center">Date</th>
                            <th class="msb-txt-center">Day</th>
                            <th class="msb-txt-center">Employee ID</th>
                            <th class="msb-txt-center">Employee Name</th>
                            <th class="msb-txt-center">Shift</th>
                            <th class="msb-txt-center">In Time</th>
                            <th class="msb-txt-center">Late</th>
                            <th class="msb-txt-center">Out Time</th>
                            <th class="msb-txt-center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach($attendance_data as $data)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$data['entry_date']}}</td>
                            <td class="msb-txt-center">{{$data['day']}}</td>
                            <td class="msb-txt-center">{{$data['employee_id']}}</td>
                            <td class="msb-txt-center">{{$data['employee_name']}}</td>
                            <td class="msb-txt-center">{{$data['shift']}}</td>
                            <!-- <td style="text-align: center;">{{$data['start_time']}}</td> -->
                            <td class="msb-txt-center">
                            @if(!empty($data['in_time']))
                                {{date('h:i a', strtotime($data['in_time']))}}
                            @endif
                            </td>
                            <td class="msb-txt-center msb-txt-red">{{$data['late']}}</td>
                            <!-- <td style="text-align: center;">{{$data['end_time']}}</td> -->
                            <td class="msb-txt-center">
                            @if(!empty($data['out_time'])&&($data['in_time']!=$data['out_time']))
                                {{date('h:i a', strtotime($data['out_time']))}}
                            @else

                                @if(!empty($data['in_time']))
                                    <span class="msb-txt-red">Not Found</span>
                                @endif
                                
                            @endif
                            </td>
                            <td class="msb-txt-center">
                                <?php 
                                    if ($data['status'] == 'Absent') {
                                        echo '<span class="msb-bg-red msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }
                                    
                                    if ($data['status'] == 'Late') {
                                        echo '<span class="msb-bg-late msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present-Weekend') {
                                        echo '<span class="msb-bg-green msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Weekend') {
                                        echo '<span class="msb-bg-weekend msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present') {
                                        echo '<span class="msb-bg-green msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Leave') {
                                        echo '<span class="msb-bg-leave msb-txt-white msb-pd-3">'.$data['status'].'</span>';
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div>

                </div>
            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection

@section('extra_js')
@endsection
