@extends('layout.master')
@section('title','Notification Message')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> {{Session::get("messageTitle")}}</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <div class="alert alert-teal">
                    <h4>{{Session::get("messageSuccess")}}</h4>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
@endsection
