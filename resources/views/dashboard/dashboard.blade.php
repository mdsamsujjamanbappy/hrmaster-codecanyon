@extends('layout.master')
@section('title','Dashboard')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle">
        <i class="icon ion-ios-home-outline"></i>
        <div>
            <h4>Dashboard</h4>
        </div>
    </div>

    <div class="br-pagebody">
        <?php if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) || checkPermission(['company_director']) || checkPermission(['company_admin']) || checkPermission(['company_hr'])){ ?>

        <div class="row row-sm row-cards-one">
            <div class="col-sm-6 col-xl-3">
                <div class="mycard bg7 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fa fa-building tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%02d", $company_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Company</span>
                        </div>
                    </div>
                    <div id="ch1" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
                <div class="mycard bg1 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fa fa-users tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $employee_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Employees</span>
                        </div>
                    </div>
                    <div id="ch3" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg4 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-th-list tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $department_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Employee Department</span>
                        </div>
                    </div>
                    <div id="ch2" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg3 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fab fa-black-tie tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $designation_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Employee Designation</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm mg-t-20  row-cards-one">
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg5 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-database tx-60 lh-0 tx-white op-9"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $leave_type_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Leave Type</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg6 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-layer-group tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $work_shift_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Work Shift</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg2 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-layer-group tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $pending_leave_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Pending Leave</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg1 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-layer-group tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $approved_leave_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Approved Leave</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->
        
        <div class="row row-sm mg-t-20  row-cards-one">
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg7 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-award tx-60 lh-0 tx-white op-9"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $employee_award_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Employee Awards</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg4 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-bullhorn tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $notice_list_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Notice</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->

            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg3 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-dice-d20 tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $expense_category_count)}}</p>
                            <span class="tx-15 tx-roboto tx-white">Expense Category</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="mycard bg6 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Today's Expense</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">@money(sprintf("%03d", $todays_expense))</p>
                            <span class="tx-12 tx-roboto tx-white">Date: {{date('d-M-Y')}}</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->

    <?php }else{ ?>
        <hr>
        <h1 class="pd-t-20">Welcome <b>{{Auth::user()->name}}</b></h1>
        <h3>Today is <b>{{date('F d, Y')}}</b> and it's <b>{{date('l')}}</b></h3>
    <?php } ?>

    </div>
@endsection

@section('extra_js')
@endsection