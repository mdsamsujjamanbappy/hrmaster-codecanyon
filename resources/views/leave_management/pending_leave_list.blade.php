@extends('layout.master')
@section('title','Pending Leave Request List')
@section('extra_css')
    {{ Html::style('theme/css/leave-management.css') }}
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Pending Leave Request List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered stripe table-hover" >
                    <thead>
                        <tr>
                            <th class="msb-bg-table-td">SN</th>
                            <th class="msb-bg-table-td">Employee ID</th>
                            <th class="msb-bg-table-td">Employee Name</th>
                            <th class="msb-bg-table-td">Company Name</th>
                            <th class="msb-txt-center">Leave Type</th>
                            <th class="msb-txt-center">Leave Start Date</th>
                            <th class="msb-txt-center">Leave End Date</th>
                            <th class="msb-txt-center">Total Days</th>
                            <th class="msb-txt-center">Request Date</th>
                            <th class="msb-txt-center">Attachment</th>
                            <th class="msb-txt-center">Remarks</th>
                            <th class="msb-txt-center">Status</th>
                            <th class="msb-txt-center msb-bg-table-td">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($leave_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->employee_main_id}}</td>
                            <td class="msb-txt-center">{{$item->emp_first_name}} {{$item->emp_last_name}}</td>
                            <td class="msb-txt-center">{{$item->company_name}}</td>
                            <td class="msb-txt-center">{{$item->leave_type_name}}</td>
                            <td class="msb-txt-center">@if(!empty($item->leave_starting_date)) {{date('d-m-Y', strtotime($item->leave_starting_date))}} @endif</td>
                            <td class="msb-txt-center">@if(!empty($item->leave_ending_date)) {{date('d-m-Y', strtotime($item->leave_ending_date))}} @endif</td>
                            <td class="msb-txt-center">{{$item->actual_days}}</td>
                            <td class="msb-txt-center">@if(!empty($item->created_at)) {{date('d-m-Y', strtotime($item->created_at))}} @endif</td>
                            <td class="msb-txt-center">-</td>
                            <td class="msb-txt-center">{{$item->description}}</td>
                            <td class="msb-txt-center">
                                <?php 
                                    if($item->status==0){
                                        echo "<span class='msb-txt-blue'>Pending</span>";
                                    }elseif($item->status==1){
                                        echo "<span class='msb-txt-green'>Approved</span>";
                                    }elseif($item->status==2){
                                        echo "<span class='msb-txt-red'>Rejected</span>";
                                    }
                                ?>
                            </td>
                            <td class="msb-txt-center msb-bg-table-td">
                                <a class="btn btn-sm btn-success" href="{{route('employee.leave.pending.approve', base64_encode($item->id))}}" onclick="return confirm('Are you sure to Approve?')" title="Approve"><i class="fa fa-check-square"></i></a>
                                <a class="btn btn-sm btn-danger" href="{{route('employee.leave.pending.reject', base64_encode($item->id))}}" onclick="return confirm('Are you sure to Reject?')" title="Reject"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
@section('extra_js')
@endsection



