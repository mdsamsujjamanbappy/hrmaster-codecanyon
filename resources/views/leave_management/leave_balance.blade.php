@extends('layout.master')
@section('title','Leave Balance List')
@section('extra_css')
    {{ Html::style('theme/css/leave-management.css') }}
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Leave Balance List</h4>
        </div>
        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table table-bordered stripe table-hover" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center msb-bg-table-td" rowspan="2">SN</th>
                            <th class="msb-txt-center msb-bg-table-td" rowspan="2">Employee ID</th>
                            <th class="msb-txt-center msb-bg-table-td" rowspan="2">Employee Name</th>
                            <th class="msb-txt-center msb-bg-table-td" rowspan="2">Company Name</th>
                            @foreach($leave_type_setting as $item)
                             <th colspan="3" class="msb-txt-center">{{$item->leave_type_name}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($leave_type_setting as $item)
                             <th class="msb-txt-center msb-bg-lb-total">Total</th>
                             <th class="msb-txt-center msb-bg-lb-taken">Taken</th>
                             <th class="msb-txt-center msb-bg-lb-available">Available</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($employee_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->employee_id}}</td>
                            <td class="msb-txt-center">{{$item->emp_first_name}} {{$item->emp_last_name}}</td>
                            <td class="msb-txt-center">{{$item->company_name}}</td>
                            @foreach($leave_type_setting as $lts)
                            <?php 
                                $leaveLists = DB::table('tb_employee_leave_application')
                                ->where([['employee_id', '=', $item->id], ['leave_type_id', '=', $lts->id]])
                                ->whereYear('leave_starting_date', date('Y'))
                                ->where('status', '=', 1)->sum('actual_days');
                             ?>
                             <td class="msb-txt-center msb-bg-lb-total">{{$lts->total_leave_days}}</td>
                             <td class="msb-txt-center msb-bg-lb-taken">{{$leaveLists}}</td>
                             <td class="msb-txt-center msb-bg-lb-available">{{(($lts->total_leave_days)-($leaveLists))}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection



