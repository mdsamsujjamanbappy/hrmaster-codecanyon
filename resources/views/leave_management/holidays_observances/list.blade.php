@extends('layout.master')
@section('title','Holidays Observances List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Holidays/Observances List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered " >
                    <thead>
                        <tr>
                            <th width="5%">SN</th>
                            <th>Company Name</th>
                            <th>Title</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Leave Days</th>
                            <th>Remarks</th>
                            <th>Status</th>
                            <th width="15%" class="msb-txt-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($holidays_list as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->company_name}}</td>
                            <td>{{$item->holiday_title}}</td>
                            <td>{{date('d-m-Y', strtotime($item->start_date))}}</td>
                            <td>{{date('d-m-Y', strtotime($item->end_date))}}</td>
                            <td>
                            <?php 
                            $earlier = new DateTime($item->start_date);
                            $later = new DateTime($item->end_date);

                            $diff = $later->diff($earlier)->format("%a");
                            echo ++$diff;
                            ?>
                            </td>
                            <td>{{$item->remarks}}</td>
                            <td>
                                @if($item->status==1)
                                    <span class="msb-txt-green;">Active</span>
                                @else
                                    <span class="msb-txt-red;">Inactive</span>
                                @endif
                            </td>
                            <td class="msb-txt-center">
                                <button type="button" value="{{base64_encode($item->id)}}" class="edit btn btn-sm btn-info" title="Edit Information"><i class="fa fa-edit"></i></button>
                                <a class="btn btn-sm btn-danger" href="{{route('settings.leave.holidays_observances.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div id="add_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Holiday/Observance </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'settings.leave.holidays_observances.store']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>New Holiday/Observance Information</h6>
                                        <hr>
                                    </div>

                                    <div class="col-sm-12 col-12">
                                        <div class="form-group row select_2_row_modal">
                                            <label for="department" class="col-sm-4 control-label form-label-1">Company <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="company_id" id="company_id" data-placeholder="Company" required="">
                                                      <option value="">Select Company</option>
                                                    @foreach($company_list as $item)
                                                      <option value='{{ $item->id }}'>{{ $item->company_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Holiday Title <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="holiday_title" autocomplete="off" value="" required="" placeholder="Title">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Leave From <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control fc-datepicker" name="start_date" autocomplete="off" value="" required="" placeholder="Leave From">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Leave To <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control fc-datepicker" name="end_date" autocomplete="off" value="" required="" placeholder="Leave To">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="select_2" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option selected="" value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><br></div>

                                <div class="form-group text-center col-sm-12">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->


            <div id="edit_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-edit "></i> Edit Holiday/Observance Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'settings.leave.holidays_observances.update']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Holiday/Observance Information</h6>
                                    <hr>

                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group row select_2_row_modal">
                                            <label for="department" class="col-sm-4 control-label form-label-1">Company <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="company_id" id="company_id_edit" data-placeholder="Company" required="">
                                                      <option value="">Select Company</option>
                                                    @foreach($company_list as $item)
                                                      <option value='{{ $item->id }}'>{{ $item->company_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Holiday Title <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="holiday_title" id="holiday_title_edit" autocomplete="off" value="" required="" placeholder="Title">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Leave From <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control fc-datepicker" name="start_date" id="start_date_edit" autocomplete="off" required="" placeholder="Leave From">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Leave To <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control fc-datepicker" name="end_date" autocomplete="off" value="" required="" id="end_date_edit" placeholder="Leave To">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks_edit" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="status_edit" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><br /></div>

                                <div class="form-group text-center col-sm-12">
                                    <input type="hidden" name="id" id="edit_id">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Update </button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        'use strict';

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('value');
            $.ajax({
             type: "GET",
             url:"{{url('settings/leave/holidays_observances/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 // console.log(response);
                 $("#edit_modal").modal('show');
                 $('#edit_id').val(response.id);
                 $('#company_id_edit').val(response.company_id);
                 $('#holiday_title_edit').val(response.holiday_title);
                 $('#start_date_edit').val(response.start_date);
                 $('#end_date_edit').val(response.end_date);
                 $('#remarks_edit').val(response.remarks);
                 $('#status_edit').val(response.status);
             },
                error:function(response){
                    console.log(response);
                },
            })
        });
    });
</script>
@endsection



