@extends('layout.master')
@section('title','Assign Leave')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle">
    </div>
    <div class="form_main_body">
        <div class="col-md-12 mt-3">
            <div class="card card_main_body">
                <div class="card-header">
                    <p>
                        <i class="fas fa-plus-circle"></i>
                        Assign Leave
                    </p>
                </div>

                <div class="card-body">
                    {!! Form::open(['method'=>'POST','route'=>'employee.leave.assign.store', 'files'=>true]) !!}
                    <div class="form-group">

                         <div class="row">
                        <div class="col-md-4">
                            <label for="company_id" class="control-label form-label-1">Select Company<span class='msb-txt-red'>*</span></label>
                            <div>
                                <select id="company_id" name="company_id"  class="form-control">
                                    <option value="">Select Company</option>
                                        @foreach($company_list as $item)
                                        <option value="{{$item->id}}">{{$item->company_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="department_id" class="control-label form-label-1">Select Department<span class='msb-txt-red'>*</span></label>
                            <div>
                                <select id="department_id" name="department_id"  class="form-control">
                                    <option value="">All Department</option>
                                        @foreach($department_list as $dept)
                                        <option value="{{$dept->id}}">{{$dept->department_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="employee_id" class="control-label form-label-1">Select Employee<span class='msb-txt-red'>*</span></label>
                            <div class="">
                                <select id="employee_id" name="employee_id"  class="form-control">
                                    <option value="">Select Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="leave_type_id" class="control-label form-label-1">Select Leave Type<span class='msb-txt-red'>*</span></label>
                            <div>
                                <select id="leave_type_id" name="leave_type_id"  class="form-control">
                                    <option value="">Select Employee First</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="task_assign_date" class="control-label form-label-1">Leave Start Date<span class='msb-txt-red'>*</span></label>
                            <div class='input-group datetimepicker-disable-date date'  id='leave_start_date' >
                                <input type='date' class="form-control" id="leave_starting_date" name="leave_starting_date"/>
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>

                            </div>
                            <b class="form-text text-danger pull-left" id="startdateError"></b>
                        </div>
                        <div class="col-md-4">
                            <label for="task_assign_date" class="control-label form-label-1">Leave End Date<span class='msb-txt-red'>*</span></label>
                            <div class='input-group datetimepicker-disable-date date'  id='leave_end_date' >
                                <input type='date' class="form-control" id="leave_ending_date" name="leave_ending_date"/>
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>

                            </div>
                            <b class="form-text text-danger pull-left" id="enddateError"></b>
                        </div>
                        <div class="col-md-4">
                            <label for="actual_days" class="control-label form-label-1">Total Days</label>
                            <div>
                                <input class="form-control" type="text" name="actual_days" id="actual_days" readonly>
                                <b class="form-text text-danger pull-left" id="totalDaysError"></b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="attachment" class="control-label form-label-1">Attachment</label>
                            <div>
                                <input class="form-control" type="file" name="attachment" id="attachment">
                            </div>
                        </div>

                        <div class="col-md-4">
                             <label for="" class="control-label form-label-1">Reason</label>
                            <div>
                                <textarea  class="form-control" name="description" id="description" cols="9" rows="2" style="width:100%;" placeholder=" Add Description"></textarea>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="form-group ">
                        <hr>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 msb-txt-center">
                            <button type="submit" id="assign_btn" class="btn btn-info btn-sm custom-btn-1 ml-2" style=""><i class="fa fa-check"></i> Assign Leave</button>
                        </div>
                    </div>
                   {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')

<script>
    $(document).ready(function(){
    'use strict';

        //get employee start
        $('#company_id').on('change',function () {
            var company_id = $("#company_id").val();
            var department_id = $("#department_id").val();
            if(!department_id){
                department_id='all';
            }
            if(company_id){
                $.ajax({
                    type: "GET",
                    url:"{{url('/ajax/get_employee/company_department')}}"+"/"+company_id+"/"+department_id,
                    success:function (response) {
                        $('#employee_id').html(response);
                        $("#employee_id").select2({
                            placeholder: "Select Employee"
                        });
                    },
                    error:function(response){
                      console.log(response);
                    }
                });
            }
        });

        //get employee end
        $('#department_id').on('change',function () {
            $('#company_id').change();
        });

        //get leave type start
        $('#employee_id').on('change',function () {
            var id = $("#employee_id").val();
            //alert(id);
            $.ajax({
                type: "GET",
                url:"{{url('/ajax/assign_leave/get_leave_type/')}}"+"/"+id,
                success:function (response) {
                    //console.log(response);
                    $('#leave_type_id').html(response);
                    $("#leave_type_id").select2({
                        placeholder: "Select Leave Type"
                    });
                },
                error:function(response){
                  console.log(response);
                }
            });

            //lode assigned task list
            //var id = $("#employee_id").val();
            $.ajax({
                method:"get",
                url:"{{url('/leave/assigned/list')}}"+"/"+id,
                success:function (response) {
                    if(response.error)
                    {
                        var assigned_leave = $('#assigned_leave').DataTable();
                        assigned_leave.clear().draw();
                        $('#errorMssg').show();
                        $('#errorMssg').html(response.error);
                    }
                    if(response.success)
                    {
                        $('#errorMssg').hide();
                        var assigned_leave = $('#assigned_leave').DataTable();
                        assigned_leave.clear();
                        var no = 1;
                        $.each(response.success,function(i, data){
                            //console.log(data);
                                assigned_leave.row.add([
                                    no++,
                                    data.emp_name,
                                    data.leave_type,
                                    data.leave_starting_date,
                                    data.leave_ending_date,
                                    data.actual_days,
                                    data.approve_by,
                                ]).draw(true);
                        })
                    }
                },
                error:function(response){
                  console.log(response);
                }
            });
            //lode assigned task list end

        });
        //get leave type end

        //calculate days on change date start
        $('#leave_type_id').on('change',function () {
            $("#totalDaysError").html('');
            $('.leave_info').show();
            $("#assign_btn").prop('disabled', false);
            var leave_type = $('#leave_type_id').val();

                $("#leave_ending_date").val('');
                $("#leave_ending_date").prop("readonly", false);

                $('#leave_ending_date').change(function() {

                    var start = $("#leave_starting_date").val();
                    var end = $("#leave_ending_date").val();

                    var end_date = new Date(end);
                    end_date.setDate(end_date.getDate() + 1);

                    var diff = end_date - new Date(start);
                    var days = diff / 1000 / 60 / 60 / 24;

                    //check available leave
                    var e_id = $("#employee_id").val();
                    $.ajax({
                        type: "GET",
                        url:"{{url('/ajax/assign_leave/get_employee_available_leave')}}"+"/"+leave_type+"/"+e_id,
                        success:function (response) {
                            //console.log(response.maternity);
                            //$('#leave_type_id').html(response);
                            if(response.maternity){
                                $("#actual_days").val('');
                                $("#actual_days").css('border-color', '#a94442');
                                $("#totalDaysError").html('* '+response.maternity);
                                swal(response.maternity, "", "warning");
                                $("#leave_ending_date").val('');
                                $("#assign_btn").prop('disabled', true);
                            }
                            if(response.error){
                                $("#actual_days").val('');
                                $("#actual_days").css('border-color', '#a94442');
                                $("#totalDaysError").html('* '+response.error);
                                swal(response.error, "", "warning");
                                $("#leave_ending_date").val('');
                                $("#assign_btn").prop('disabled', true);
                            }
                            if(response.leave){
                                if(days > response.leave){
                                    $("#actual_days").val('');
                                    $("#actual_days").css('border-color', '#a94442');
                                    $("#totalDaysError").html('* Enough Leave Is not available');
                                    swal('Available Leave is '+response.leave+ ' days', "", "warning");
                                    $("#leave_ending_date").val('');
                                    $("#assign_btn").prop('disabled', true);
                                }
                                else{
                                    $("#totalDaysError").html('');
                                    $("#actual_days").val(days);
                                    $("#actual_days").css('border-color', '#32CD32');
                                    $("#assign_btn").prop('disabled', false);
                                }
                            }

                        }
                    });
                });


        });
        //calculate days on change date end

        //start assign leave
         $( "#assign_btn" ).click(function() {
            var _token = '{{ csrf_token() }}';
            var employee_id = $("#employee_id").val();
            var task_id = $("#task_id").val();
            var assign_date = $("#assign_date").val();

            $("#attachment").val('');
            //alert();

        });

        //add task
        $('#assign_leave_form').on('submit', function(event){
            event.preventDefault();
                $("#startdateError").html('');
                $("#enddateError").html('');
                var leave_start_date = $("#leave_starting_date").val();
                var leave_ending_date = $("#leave_ending_date").val();
                if( leave_start_date == '' ){
                    //alert(leave_start_date);
                    $("#leave_start_date").css('border-color', '#a94442');
                    $("#startdateError").html('* Leave Start Date Is required');
                    return false;
                }else if( leave_ending_date == '' ){
                    //alert(leave_start_date);
                    $("#leave_ending_date").css('border-color', '#a94442');
                    $("#enddateError").html('* Leave End Date Is required');
                    return false;
                }
                else{
                }

           });
        //end assign leave
    });
    </script>
@endsection




