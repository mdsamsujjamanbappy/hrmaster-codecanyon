@extends('layout.master')
@section('title','Expense History')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Expense History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered table-hover" >
                    <thead>
                        <tr>    
                            <th class="msb-txt-center msb-bg-table-td" >SN</th>
                            <th class="msb-txt-center msb-bg-table-td" >Category Name</th>
                            <th class="msb-txt-center msb-bg-table-td" >Expense Date</th>
                            <th class="msb-txt-center msb-bg-table-td" >Expense Amount</th>
                            <th class="msb-txt-center msb-bg-table-td" width="15%" >Remarks</th>
                            <th class="msb-txt-center msb-bg-table-td" >Attachment</th>
                            <th class="msb-txt-center msb-bg-table-td" >Status</th>
                            <th class="msb-txt-center msb-bg-table-td" >Created By</th>
                            <th class="msb-txt-center msb-bg-table-td" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; $total_amount=0; @endphp
                    @foreach($expense_list as $item)
                    @php $total_amount += $item->amount; @endphp
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->category_name}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->expense_date))}}</td>
                            <td class="msb-txt-center">@money($item->amount)</td>
                            <td class="msb-txt-center">{{$item->remarks}}</td>
                            <td class="msb-txt-center">
                                @if(!empty($item->attachment))
                                    <a href="{{asset('expense_attachment/'.$item->attachment)}}" target="new" class=""><i class="fa fa-download"></i> Download</a>
                                @endif
                            </td>
                            <td class="msb-txt-center">@if($item->status==1) Approved @elseif($item->status==0) Pending @endif</td>
                            <td class="msb-txt-center">{{$item->name}}</td>
                            <td class="msb-txt-center">
                                <a class="msb-txt-red msb-txt-fs-14" href="{{route('expense_list.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" target="_BLANK" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3" class="msb-txt-center msb-bg-table-td msb-txt-fs-14">Total</th>
                            <th class="msb-txt-center msb-bg-table-td msb-txt-fs-14">@money($total_amount)</th>
                            <th class="msb-txt-center msb-bg-table-td" ></th>
                            <th class="msb-txt-center msb-bg-table-td" ></th>
                            <th class="msb-txt-center msb-bg-table-td" ></th>
                            <th class="msb-txt-center msb-bg-table-td" ></th>
                            <th class="msb-txt-center msb-bg-table-td" ></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
