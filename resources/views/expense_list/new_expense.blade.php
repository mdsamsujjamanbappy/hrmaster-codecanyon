@extends('layout.master')
@section('title','New Expense')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Expense</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'expense_list.new.store', 'files' =>true]) !!}
                    <div class="row">
                
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Category Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="expense_category_id" id="expense_category_id" required="">
                                        <option value="">Select Category</option>
                                        @foreach($expense_category as $item)
                                          <option value="{{$item->id}}">{{$item->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Expense Date <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-datepicker" name="expense_date" autocomplete="off" placeholder="Expense Date" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Expense Amount<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="number" step="step" class="form-control" name="amount" autocomplete="off" placeholder="Expense Amount" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Expense Attachment</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="attachment" autocomplete="off" placeholder="Expense Attachment">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Expense Remarks</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea class="form-control" rows="6" name="remarks" autocomplete="off" placeholder="Expense Remarks"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-8">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
