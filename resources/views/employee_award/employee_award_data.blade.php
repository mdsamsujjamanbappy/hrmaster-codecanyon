@extends('layout.master')
@section('title','Award History')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Award History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered table-hover " >
                    <thead>
                        <tr>    
                            <th class="msb-txt-center msb-bg-table-td" >SN</th>
                            <th class="msb-txt-center msb-bg-table-td" >Employee Name</th>
                            <th class="msb-txt-center msb-bg-table-td" >Employee ID</th>
                            <th class="msb-txt-center msb-bg-table-td" >Designation Name</th>
                            <th class="msb-txt-center msb-bg-table-td" >Award Name</th>
                            <th class="msb-txt-center msb-bg-table-td" >Gift</th>
                            <th class="msb-txt-center msb-bg-table-td" >Cash Price</th>
                            <th class="msb-txt-center msb-bg-table-td" >Award Month</th>
                            <th class="msb-txt-center msb-bg-table-td" width="12%" >Remarks</th>
                            <th class="msb-txt-center msb-bg-table-td" >Attachment</th>
                            <th class="msb-txt-center msb-bg-table-td" >Created By</th>
                            <th class="msb-txt-center msb-bg-table-td" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($award_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->emp_first_name}} {{$item->emp_last_name}}</td>
                            <td class="msb-txt-center">{{$item->employee_id}}</td>
                            <td class="msb-txt-center">{{$item->designation_name}}</td>
                            <td class="msb-txt-center">{{$item->award_name}}</td>
                            <td class="msb-txt-center">{{$item->award_gift}}</td>
                            <td class="msb-txt-center">@money($item->cash_price)</td>
                            <td class="msb-txt-center">{{date('M-Y', strtotime($item->award_period))}}</td>
                            <td class="msb-txt-center">{{$item->remarks}}</td>
                            <td class="msb-txt-center">
                                @if(!empty($item->award_attachment))
                                    <a href="{{asset('award_attachment/'.$item->award_attachment)}}" target="new" class=""><i class="fa fa-download"></i> Download</a>
                                @endif
                            </td>
                            <td class="msb-txt-center">{{$item->name}}</td>
                            <td class="msb-txt-center">
                                <a class="msb-txt-red msb-txt-fs-14" href="{{route('employee_award.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" target="_BLANK" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
