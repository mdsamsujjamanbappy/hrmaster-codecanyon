@extends('layout.master')
@section('title','New Award')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Award</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'employee_award.new.store', 'files' =>true]) !!}
                    <div class="row">
                
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Employee <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="employee_id" id="employee_id" required="">
                                        <option value="">Select Employee</option>
                                        @foreach($employee_list as $item)
                                          <option value="{{$item->id}}">{{$item->emp_first_name}} {{$item->emp_last_name}} ({{$item->employee_id}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Award Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="award_name" autocomplete="off" placeholder="Award Name" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Award Gift <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="award_gift" autocomplete="off" placeholder="Award Gift" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Cash Price <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="number" step="any" class="form-control" name="cash_price" autocomplete="off" placeholder="Cash Price" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Award Period<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-monthpicker" name="award_period" autocomplete="off" placeholder="Award Period" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Award Attachment</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="attachment" autocomplete="off" placeholder="Award Attachment">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Remarks</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea class="form-control" rows="6" name="remarks" autocomplete="off" placeholder="Remarks"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-8">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
