<div id="employee_education_modal" class="modal fade effect-sign">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Educational Information </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <span id="form_result"></span>
                <div class="modal_body_inner">
                    {!! Form::open(['method'=>'POST','route'=>'employee_education.store','files'=>true]) !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1"> Exam Title <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_exam_title" autocomplete="off"  required="" placeholder="Examination Title">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Institution <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="emp_institution_name"  autocomplete="off" required="" placeholder="Institution">
                                    </div>
                                </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Result <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="number" step="any" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="emp_result" autocomplete="off" required="" placeholder="Result" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Scale <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input  type="number" step="any" name="emp_scale" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;"  class="form-control pd-y-12" autocomplete="off" required="" placeholder="Scale">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Passing Year <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" name="emp_passing_year" class="form-control pd-y-12 date-own form-control" autocomplete="off" required="" placeholder="Passing Year" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Attachment</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" name="emp_attachment" class="form-control pd-y-12" autocomplete="off"  placeholder="Attachment">
                                </div>
                            </div>
                          </div>
                        </div>
                         <input type="hidden" name="ref_id" value="{{($employee->id)}}">
                         <div class="form-group text-center col-sm-12">
                            <hr>
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit">Save Information</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>

                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
