<div id="update_modal" class="modal fade effect-sign">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Employee Information </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <span id="form_result"></span>
                <div class="modal_body_inner">
                    {!! Form::open(['method'=>'POST','route'=>'employee.information.update']) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1"> Employee ID <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="employee_id" autocomplete="off" value="{{$employee->employee_id}}" required="" placeholder="Employee ID">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">First Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_first_name" value="{{$employee->emp_first_name}}" autocomplete="off" required="" placeholder="Employee First Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Last Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_last_name" value="{{$employee->emp_last_name}}" autocomplete="off" required="" placeholder="Employee Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Father Name </label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_father_name" value="{{$employee->emp_father_name}}" autocomplete="off" placeholder="Employee Father Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Mother Name </label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_mother_name" value="{{$employee->emp_mother_name}}" autocomplete="off" placeholder="Employee Mother Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Email <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="email" class="form-control" name="emp_email" value="{{$employee->emp_email}}"  autocomplete="off" required="" placeholder="Email">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Phone <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="emp_phone" value="{{$employee->emp_phone}}"  autocomplete="off" required="" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Gender <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_gender_id" required="">
                                        <option></option>
                                        @foreach($gender_list as $item)
                                            <option @if($item->id==$employee->emp_gender_id) selected @endif value="{{$item->id}}">{{$item->gender_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="department" class="control-label form-label-1">Company <span class="msb-txt-red">*</span></label>
                                </div>

                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_company_id" id="emp_company_id_edit" data-placeholder="department">
                                          <option value="">Select Company</option>
                                          @foreach($company_list as $item)
                                          <option value='{{ $item->id }}' @if($item->id == $employee->company_id) selected @endif >{{ $item->company_name }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-6 col-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="department" class="control-label form-label-1">Department <span class="msb-txt-red">*</span></label>
                                </div>

                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_department_id" id="emp_department_id_edit" data-placeholder="department">
                                          <option value="">Select Department</option>
                                          @foreach($department_list as $item)
                                          <option value='{{ $item->id }}' @if($item->id == $employee->emp_department_id) selected @endif >{{ $item->department_name }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                         <div class="col-sm-6 col-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="designation" class="control-label form-label-1">Designation <span class="msb-txt-red">*</span></label>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_designation_id" id="emp_designation_id_edit" data-placeholder="designation">
                                            <option value="">Select Designation</option>
                                            @foreach($designation_list as $item)
                                            <option value='{{ $item->id }}' @if($item->id == $employee->emp_designation_id) selected @endif >{{ $item->designation_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4  control-label form-label-1">Birth Date</label>
                                <div class="col-sm-8 pl-0">
                                    <input autocomplete="off"  type="text" name="emp_dob" value="{{$employee->emp_dob}}" class="form-control profile_update_picker" placeholder="DD/MM/YYYY" >

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4  control-label form-label-1">Joining Date</label>
                                <div class="col-sm-8 pl-0">
                                    <input autocomplete="off"  type="text" value="{{$employee->emp_joining_date}}" name="emp_joining_date" class="form-control profile_update_picker1" placeholder="YYYY-MM-DD">

                                </div>
                            </div>
                        </div>
                         <div class="col-sm-6 col-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="designation" class="control-label form-label-1">Blood Group</label>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_blood_group" id="emp_blood_group" data-placeholder="emp_blood_group">
                                            <option value="">Select a Blood Group</option>
                                           
                                            <option value='A+' <?php if('A+' == $employee->emp_blood_group){ echo "selected"; } ?> >A+</option>
                                            <option value='A-' <?php if('A-' == $employee->emp_blood_group){ echo "selected"; } ?> >A-</option>
                                            <option value='B+' <?php if('B+' == $employee->emp_blood_group){ echo "selected"; } ?> >B+</option>
                                            <option value='B-' <?php if('B-' == $employee->emp_blood_group){ echo "selected"; } ?> >B-</option>
                                            <option value='AB+' <?php if('AB+' == $employee->emp_blood_group){ echo "selected"; } ?> >AB+</option>
                                            <option value='AB-' <?php if('AB-' == $employee->emp_blood_group){ echo "selected"; } ?> >AB-</option>
                                            <option value='O+' <?php if('O+' == $employee->emp_blood_group){ echo "selected"; } ?> >O+</option>
                                            <option value='O-' <?php if('O-' == $employee->emp_blood_group){ echo "selected"; } ?> >O-</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                         <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="designation" class="control-label form-label-1">Religion</label>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_religion" id="emp_religion" data-placeholder="emp_religion">
                                        <option value="">Select a Religion</option>
                                        <option <?php if('Islam' == $employee->emp_religion){ echo "selected"; } ?> value='Islam'>Islam</option>
                                        <option <?php if('Hinduism' == $employee->emp_religion){ echo "selected"; } ?> value='Hinduism'>Hinduism</option>
                                        <option <?php if('Buddhists' == $employee->emp_religion){ echo "selected"; } ?> value='Buddhists'>Buddhists</option>
                                        <option <?php if('Christians' == $employee->emp_religion){ echo "selected"; } ?> value='Christians'>Christians</option>
                                        <option <?php if('Animists' == $employee->emp_religion){ echo "selected"; } ?> value='Animists'>Animists</option>
                                        <option <?php if('Others' == $employee->emp_religion){ echo "selected"; } ?> value='Others'>Others</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="designation" class="control-label form-label-1">Marital Status</label>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_marital_status" id="emp_marital_status" data-placeholder="emp_marital_status">
                                        <option value="">Select a Marital Status</option>
                                        <option <?php if('Single' == $employee->emp_marital_status){ echo "selected"; } ?> value='Single'>Single</option>
                                        <option <?php if('Married' == $employee->emp_marital_status){ echo "selected"; } ?> value='Married'>Married </option>
                                        <option <?php if('Divorced' == $employee->emp_marital_status){ echo "selected"; } ?> value='Divorced'>Divorced</option>
                                        <option <?php if('Separated' == $employee->emp_marital_status){ echo "selected"; } ?> value='Separated'>Separated</option>
                                        <option <?php if('Widowed' == $employee->emp_marital_status){ echo "selected"; } ?> value='Widowed'>Widowed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <div class="col-sm-4 text-right">
                                    <label for="designation" class="control-label form-label-1">Nationality</label>
                                </div>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="emp_nationality" id="emp_nationality" data-placeholder="emp_nationality">
                                        <option value="">Select a Nationality</option>
                                        <option <?php if('Bangladeshi' == $employee->emp_nationality){ echo "selected"; } ?> value='Bangladeshi'>Bangladeshi</option>
                                        <option <?php if('Indian' == $employee->emp_nationality){ echo "selected"; } ?> value='Indian'>Indian </option>
                                        <option <?php if('Chinese' == $employee->emp_nationality){ echo "selected"; } ?> value='Chinese'>Chinese</option>
                                        <option <?php if('USA' == $employee->emp_nationality){ echo "selected"; } ?> value='USA'>USA</option>
                                        <option <?php if('UK' == $employee->emp_nationality){ echo "selected"; } ?> value='UK'>UK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Attendance Card Number</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="{{$employee->emp_card_number}}" name="emp_card_number" autocomplete="off" placeholder="Attendance Card Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">National ID</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="{{$employee->emp_nid}}" name="emp_nid" autocomplete="off" placeholder="National ID">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">IPBX Extension No.</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="{{$employee->emp_ipbx_extension}}" name="emp_ipbx_extension" autocomplete="off" placeholder="IPBX Extension No.">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Bank Account No.</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="{{$employee->emp_bank_account}}" name="emp_bank_account" autocomplete="off" placeholder="Bank Account Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Bank Information</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="{{$employee->emp_bank_info}}" name="emp_bank_info" autocomplete="off" placeholder="Bank Details Information">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4  control-label form-label-1">Current Address</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea name="emp_current_address" rows="3" class="form-control" placeholder="Current Address">{{$employee->emp_current_address}}</textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4  control-label form-label-1">Permanent Address</label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="emp_parmanent_address"  rows="3" class="form-control" placeholder="Permanent Address">{{$employee->emp_parmanent_address}}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4  control-label form-label-1">Account Status <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <select name="emp_account_status" class="form-control " id="emp_account_status" required>
                                            @if($employee->emp_account_status==1)
                                                <option value="1" selected>Active</option>
                                                <option value="0">Inactive</option>
                                            @else
                                                <option value="1">Active</option>
                                                <option value="0" selected>Inactive</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                          </div>
                         <input type="hidden" name="ref_id" value="{{($employee->id)}}">
                         <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Update Information</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
