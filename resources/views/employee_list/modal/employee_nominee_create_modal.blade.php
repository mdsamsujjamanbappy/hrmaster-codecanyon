<div id="employee_nominee_create_modal" class="modal fade effect-sign">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Create New Nominee </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <span id="form_result"></span>
                <div class="modal_body_inner">

                    <div id="img_upload_div">
                    {!! Form::open(['method'=>'POST','route'=>'employee_nominee.store', 'files'=>true]) !!}
                    <div class="row m-0 mt-3">
                        <div class="col-sm-9 m-auto">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-9 pl-0">
                                    <input type="text" name="nominee_name" id="nominee_name" class="form-control pd-y-12" autocomplete="off" required="" placeholder="Nominee name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Phone <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-9 pl-0">
                                    <input type="text" name="nominee_phone" id="nominee_phone" class="form-control pd-y-12" autocomplete="off" required="" placeholder="Nominee phone number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Relation <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-9 pl-0">
                                    <input type="text" name="nominee_relation" id="nominee_relation" class="form-control pd-y-12" autocomplete="off" required="" placeholder="Relation with nominee">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Details</label>
                                <div class="col-sm-9 pl-0">
                                    <textarea type="text" name="nominee_details" id="nominee_details" class="form-control pd-y-12" autocomplete="off" placeholder="Nominee details"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Address</label>
                                <div class="col-sm-9 pl-0">
                                    <textarea type="text" name="nominee_address" id="nominee_address" class="form-control pd-y-12" autocomplete="off" placeholder="Nominee address"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Photo</label>
                                <div class="col-sm-9 pl-0">
                                    <input type="file" name="nominee_photo"  accept="image/*"  id="nominee_photo" class="form-control pd-y-12" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Nominee Attachment</label>
                                <div class="col-sm-9 pl-0">
                                    <input type="file" name="nominee_attachment" id="nominee_attachment" class="form-control pd-y-12" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group text-center col-sm-12">
                        <hr>
                        <input type="hidden" name="ref_id" value="{{($employee->id)}}">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Save Information</button>
                        <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
