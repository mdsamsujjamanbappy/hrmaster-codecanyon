<div id="employee_employment_modal" class="modal fade effect-sign">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Employment Information </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <span id="form_result"></span>
                <div class="modal_body_inner">
                    {!! Form::open(['method'=>'POST','route'=>'employee_employment.store','files'=>true]) !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1"> Company Name <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="wh_company_name" autocomplete="off"  required="" placeholder="Company Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Designation <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="wh_designation"  autocomplete="off" required="" placeholder="Designation">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Joining Date</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control wh-datepicker" name="wh_joining_date" autocomplete="off" placeholder="Joining Date" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Resign Date</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control wh-datepicker1" name="wh_resign_date" autocomplete="off" placeholder="Resign Date" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Description</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea type="text" name="wh_description" class="form-control pd-y-12 date-own form-control" autocomplete="off" placeholder="Description" ></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Attachment</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" name="wh_attachment" class="form-control pd-y-12" autocomplete="off"  placeholder="Attachment">
                                </div>
                            </div>
                          </div>
                        </div>
                         <input type="hidden" name="ref_id" value="{{($employee->id)}}">
                         <div class="form-group text-center col-sm-12">
                            <hr>
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit">Save Information</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>

                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
