\@extends('layout.master')
@section('title','Add New Employee')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fa fa-user"></i> New Employee</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('employee.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-users"></i> Employee List</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
               {!! Form::open(['method'=>'POST','route'=>'employee.new.store']) !!}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Employee ID <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="employee_id" autocomplete="off" required="" placeholder="Employee ID">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">First Name <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_first_name" autocomplete="off" required="" placeholder="Employee First Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Last Name <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_last_name" autocomplete="off" required="" placeholder="Employee Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Father Name</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_father_name" autocomplete="off" placeholder="Employee Father Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Mother Name</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_mother_name" autocomplete="off" placeholder="Employee Mother Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1" id="requiredF1">Email</label>
                            <div class="col-sm-8 pl-0">
                                <input type="email" class="form-control" name="emp_email" id="emp_email_id"  autocomplete="off" placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-4 control-label form-label-1">Phone</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emp_phone" autocomplete="off" placeholder="Phone">
                                </div>
                            </div>
                        </div>
                    <div class="col-sm-6  col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Gender <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_gender_id" id="emp_gender_id" required=""  data-placeholder="gender">
                                    <option value="">Select Gender</option>
                                    @foreach($gender_list as $item)
                                        <option value="{{$item->id}}">{{$item->gender_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6 col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="department" class="col-sm-4 control-label form-label-1">Company <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_company_id" id="emp_company_id" data-placeholder="Company" required="">
                                      <option value="">Select Company</option>
                                      @foreach($company_list as $item)
                                      <option value='{{ $item->id }}'>{{ $item->company_name }}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-sm-6 col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="department" class="col-sm-4 control-label form-label-1">Department <span class="msb-txt-red">*</span></label>

                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_department_id" id="emp_department_id" data-placeholder="department" required="">
                                      <option value="">Select Department</option>
                                      @foreach($department_list as $item)
                                      <option value='{{ $item->id }}'>{{ $item->department_name }}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                     <div class="col-sm-6 col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="designation" class="col-sm-4 control-label form-label-1">Designation <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_designation_id" id="emp_designation_id" data-placeholder="designation" required="">
                                        <option value="">Select Designation</option>
                                        @foreach($designation_list as $item)
                                        <option value='{{ $item->id }}'>{{ $item->designation_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1">Birth Date</label>
                            <div class="col-sm-8 pl-0">
                                <input autocomplete="off"  type="text" name="emp_dob" class="form-control fc-datepicker" placeholder="YYYY-MM-DD">

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1">Joining Date</label>
                            <div class="col-sm-8 pl-0">
                                <input autocomplete="off"  type="text" name="emp_joining_date" class="form-control fc-datepicker1" placeholder="YYYY-MM-DD">

                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group row select_2_row_modal">
                            <label for="designation" class="col-sm-4 control-label form-label-1">Blood Group</label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_blood_group" id="emp_blood_group" data-placeholder="emp_blood_group">
                                    <option value="">Select a Blood Group</option>
                                    <option value='A+'>A+</option>
                                    <option value='A-'>A-</option>
                                    <option value='B+'>B+</option>
                                    <option value='B-'>B-</option>
                                    <option value='AB+'>AB+</option>
                                    <option value='AB-'>AB-</option>
                                    <option value='O+'>O+</option>
                                    <option value='O-'>O-</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group row select_2_row_modal">
                            <label for="designation" class="col-sm-4 control-label form-label-1">Religion</label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_religion" id="emp_religion" data-placeholder="emp_religion">
                                    <option value="">Select a Religion</option>
                                    <option value='Islam'>Islam</option>
                                    <option value='Hinduism'>Hinduism</option>
                                    <option value='Buddhists'>Buddhists</option>
                                    <option value='Christians'>Christians</option>
                                    <option value='Animists'>Animists</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group row select_2_row_modal">
                            <label for="designation" class="col-sm-4 control-label form-label-1">Marital Status</label>  
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_marital_status" id="emp_marital_status" data-placeholder="emp_marital_status">
                                    <option value="">Select a Marital Status</option>
                                    <option value='Single'>Single</option>
                                    <option value='Married'>Married </option>
                                    <option value='Divorced'>Divorced</option>
                                    <option value='Separated'>Separated</option>
                                    <option value='Widowed'>Widowed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group row select_2_row_modal">
                            <label for="designation" class="col-sm-4 control-label form-label-1">Nationality</label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="emp_nationality" id="emp_nationality" data-placeholder="emp_nationality">
                                    <option value="">Select a Nationality</option>
                                    <option value='Bangladeshi'>Bangladeshi</option>
                                    <option value='Indian'>Indian </option>
                                    <option value='Chinese'>Chinese</option>
                                    <option value='USA'>USA</option>
                                    <option value='UK'>UK</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Attendance Card Number</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_card_number" autocomplete="off" placeholder="Attendance Card Number">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">National ID</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_nid" autocomplete="off" placeholder="National ID">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">IPBX Extension No.</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_ipbx_extension" autocomplete="off" placeholder="IPBX Extension No.">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Bank Account No.</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_bank_account" autocomplete="off" placeholder="Bank Account Number">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Bank Information</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="emp_bank_info" autocomplete="off" placeholder="Bank Information">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1">Current Address <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <textarea name="emp_current_address" rows="3" class="form-control" placeholder="Current Address" required=""></textarea>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1">Permanent Address</label>
                            <div class="col-sm-8 pl-0">
                                <textarea name="emp_parmanent_address"  rows="3" class="form-control" placeholder="Permanent Address"></textarea>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1">Software Access <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select name="giveSoftAccessId" class="form-control giveSoftAccessId" id="giveSoftAccessId" required >
                                    <option value="" selected>Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6" id="giveSoftAccessIdField1">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1" id="requiredF2">Access Role</label>
                            <div class="col-sm-8 pl-0">
                                <select name="userType" class="form-control " id="userTypeId">
                                    <?php if(checkPermission(['super_admin']) || checkPermission(['group_admin'])){ ?>

                                    <option value="1">Super Admin</option>
                                    <option value="2">Group Admin</option>
                                    <option value="3">Group Director</option>
                                <?php } ?>

                                    <option value="4">Company Director</option>
                                    <option value="5">Company Admin</option>
                                    <!-- <option value="7">Company HR</option> -->
                                    <option value="12" selected="">Employee</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6"  id="giveSoftAccessIdField2">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4  control-label form-label-1" id="requiredF3">Password</label>
                            <div class="col-sm-8 pl-0">
                                <input type="password" name="password" class="form-control" id="passwordId" placeholder="Password">
                            </div>
                        </div>
                    </div>
                  </div>
                  <hr>
                 <div class="form-group text-center col-sm-12">
                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Save Information</button>
                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
@endsection



