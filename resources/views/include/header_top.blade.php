<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="input-group hidden-xs-down wd-170 transition">
            <input id="searchbox" type="text" class="form-control" placeholder="Search">
            <span class="input-group-btn">
            <button class="btn btn-secondary" type="button"><i class="fas fa-search"></i></button>
          </span>
        </div><!-- input-group -->
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <div class="dropdown">
                 <?php 
                    $emp_photo = "default.png";
                    if(!empty(auth()->user()->ref_id)){
                        $employee = DB::table('tb_employee_list')
                        ->select('tb_employee_list.*')
                        ->where('tb_employee_list.id', '=', auth()->user()->ref_id)
                        ->first(); 
                        
                        if(!empty($employee->emp_photo)){
                            $emp_photo = $employee->emp_photo;
                        }
                    }else{
                        $emp_photo = "default.png";
                    }
                    ?>
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">{{auth()->user()->name}}</span>
                    <img src="{{asset('employee_profile_image/'.$emp_photo)}}" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img src="{{asset('employee_profile_image/'.$emp_photo)}}" class="wd-100" alt=""></a>
                        <h6 class="logged-fullname">{{auth()->user()->name}}</h6>
                        <p>{{auth()->user()->email}}</p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        <?php if(!empty(auth()->user()->ref_id)){ ?>
                            <li><a href="{{route('employee.my_profile')}}"><i class="icon ion-ios-person"></i> My Profile</a></li>
                        <?php } ?>
                        <li><a href="{{url('logout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- br-header-right -->
</div>
