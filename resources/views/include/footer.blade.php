<div class="footer_ex pt-5 mt-5"></div>
<footer class="br-footer">
    <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; {{date('Y')}} |<b> hrMaster. </b>| All Rights Reserved.</div>
        <div></div>
    </div>
    <div class="footer-right d-flex align-items-center">
            <div class="mg-b-2">Developed By <b>MSBCSE</b></div>
    </div>
</footer>
