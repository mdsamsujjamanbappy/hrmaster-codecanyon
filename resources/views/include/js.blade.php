{{ Html::script('theme/js/jquery.min.js') }}
{{ Html::script('theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}
{{ Html::script('theme/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}
{{ Html::script('theme/lib/moment/min/moment.min.js') }}
{{ Html::script('theme/lib/peity/jquery.peity.min.js') }}
{{ Html::script('theme/js/ResizeSensor.js') }}
{{ Html::script('theme/lib/highlightjs/highlight.pack.min.js') }}
{{ Html::script('theme/lib/select2/js/select2.min.js') }}
{{ Html::script('theme/lib/datatables.net/js/jquery.dataTables.min.js') }}
{{ Html::script('theme/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}
{{ Html::script('theme/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}
{{ Html::script('theme/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}
{{ Html::script('theme/js/jquery.dataTables.min.js') }}
{{ Html::script('theme/js/sweetalert.min.js') }}
{{ Html::script('theme/js/jquery.validate.min.js') }}
{{ Html::script('theme/lib/parsleyjs/parsley.min.js') }}
{{ Html::script('theme/js/bootstrap-datepicker.js') }}
{{ Html::script('theme/js/bootstrap-timepicker.min.js') }}
{{ Html::script('theme/js/tether.min.js') }}
{{ Html::script('theme/js/moment.min.js') }}
{{ Html::script('theme/js/tempusdominus-bootstrap-4.js') }}
{{ Html::script('theme/js/bracket.js') }}
{{ Html::script('theme/js/common.js') }}
{{ Html::script('theme/js/tinymce.min.js') }}

@if(Session::has('successMessage'))
  <script type="text/javascript">
      var message = '<?php echo Session::get("successMessage"); ?>';
      swal("Success!",message, "success");
  </script>
@endif

@if(Session::has('failedMessage'))
  <script type="text/javascript">
      var message = '<?php echo Session::get("failedMessage"); ?>';
      swal("Failed!",message, "error");
  </script>
@endif
<script>
      tinymce.init({
        selector: '#mytextarea'
      });
</script>
