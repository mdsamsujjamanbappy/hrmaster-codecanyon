<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700" />
{{ Html::style('theme/lib/fontawesome5.12.1/css/all.min.css') }}
{{ Html::style('theme/lib/ionicons/css/ionicons.min.css') }}
{{ Html::style('theme/lib/select2/css/select2.min.css') }}
{{ Html::style('theme/lib/highlightjs/styles/github.css') }}
{{ Html::style('theme/css/sweetalert.min.css') }}
{{ Html::style('theme/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}
{{ Html::style('theme/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}
{{ Html::style('theme/css/bootstrap-datepicker.css') }}
{{ Html::style('theme/css/tempusdominus-bootstrap-4.css') }}
{{ Html::style('theme/css/bootstrap-timepicker.min.css') }}
{{ Html::style('theme/css/msb_theme.css') }}
{{ Html::style('theme/css/custom.css') }}
{{ Html::style('theme/css/responsive.css') }}
{{ Html::style('theme/css/animate.css') }}
{{ Html::style('theme/css/common.css') }}




