@extends('layout.master')

@section('title','Products')

@section('extra_css')


@endsection

@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Products List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <table id="zone_datatable" class="table display responsive nowrap table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th >Unit Name</th>
                        <th>Description </th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>unit-1</td>
                        <td>Loream Ipsome</td>

                        <td>
                            <button type="button" name="edit" id="" class="edit btn btn-sm btn-info "  title="Edit"><i class="fa fa-edit"></i></button>
                            <button type="button" name="view" id="view_id'" class="view btn btn-sm btn-success"  title="Delete"><i class="fas fa-eye"></i></button>
                            <button type="button" name="delete" id="delete_id" class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteCategory" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>unit-2</td>
                        <td>Loream Ipsome</td>

                        <td>
                            <button type="button" name="edit" id="" class="edit btn btn-sm btn-info "  title="Edit"><i class="fa fa-edit"></i></button>
                            <button type="button" name="view" id="view_id'" class="view btn btn-sm btn-success"  title="Delete"><i class="fas fa-eye"></i></button>
                            <button type="button" name="delete" id="delete_id" class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteCategory" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div>

    {{--    @include('designation.modal.create')--}}
    <div id="add_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Product </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST']) !!}
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Product Name</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="Username" autocomplete="off" value="" required="" placeholder="Product Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Product Code</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="Username" autocomplete="off" value="" required="" placeholder="Product Code">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Description </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="text" id="" cols="" rows="3" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="button">Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    {{--    @include('designation.modal.edit')--}}

    <div id="edit_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Unit</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST']) !!}
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Unit Name</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="Username" autocomplete="off" value="" required="" placeholder="Unit Name ">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Description </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="text" id="" cols="" rows="3" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="button">Update</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->


@endsection

@section('extra_js')

    <script>

        $(document).ready(function(){



            $('#select_2').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#add_modal .modal-content'),
                placeholder: "Select Upozilla Zone",
            });
            $('#select_3').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#edit_modal .modal-content'),
                placeholder: "Select Upozilla Zone",
            });

            $('#zone_datatable').DataTable({

            });

            //Add
            $("#designation_add_btn").on('click',function (event) {
                event.preventDefault();
                var _token = "{{ csrf_token() }}";
                var designation_name = $("#designation_name").val();
                var remarks = $("#remarks").val();
                //alert(_token);

            });

            // Edit Data
            $(document).on('click','.edit',function () {
                var id=$(this).attr('id');
                var _token="{{csrf_token()}}";
                $("#edit_modal").modal('show');

            });

            // Update
            $("#designation_update_btn").on('click',function (event) {
                event.preventDefault();
                var id=$("#designation_id").val();
                var _token="{{csrf_token()}}";
                var designation_name=$("#designation_name_edit").val();
                var remarks=$("#remarks_edit").val();
                var status=$("#status_edit").val();



            });

            //delete data

            $(document).on('click','.delete',function () {
                var id=$(this).attr('id');
                var _token="{{csrf_token()}}";
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this Designation Data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                        } else {
                            swal("Cancelled", "Your Designation file is safe :)", "error");
                        }
                    });
            });

        })








    </script>

@endsection



