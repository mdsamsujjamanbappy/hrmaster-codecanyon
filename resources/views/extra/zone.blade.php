@extends('layout.master')

@section('title','Zone')

@section('extra_css')


@endsection

@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Zone List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <table id="zone_datatable" class="table display responsive nowrap table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th >Zone Name</th>
                        <th >Address</th>
                        <th>Phone </th>
                        <th>Email</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Dhaka</td>
                            <td>Uttara</td>
                            <td>+880173858454</td>
                            <td>arif@gmail.com</td>
                            <td>
                                <button type="button" name="edit" id="" class="edit btn btn-sm btn-info "  title="Edit"><i class="fa fa-edit"></i></button>
                                <button type="button" name="view" id="view_id'" class="view btn btn-sm btn-success"  title="Delete"><i class="fas fa-eye"></i></button>
                                <button type="button" name="delete" id="delete_id" class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteCategory" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Khulna</td>
                            <td>new Road</td>
                            <td>+88017385554</td>
                            <td>jamal@gmail.com</td>
                            <td>
                                <button type="button" name="edit" id="" class="edit btn btn-sm btn-info "  title="Edit"><i class="fa fa-edit"></i></button>
                                <button type="button" name="view" id="view_id'" class="view btn btn-sm btn-success"  title="Delete"><i class="fas fa-eye"></i></button>
                                <button type="button" name="delete" id="delete_id" class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteCategory" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div>

{{--    @include('designation.modal.create')--}}
    <div id="add_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Zone </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>Zone INFO</h6>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Zone Name</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="Username" autocomplete="off" value="" required="" placeholder="Username">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Email </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="email" class="form-control" name="email" value=""  autocomplete="off" required="" placeholder="Email">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Phone </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                                <div class="form-group row select_2_row_modal">
                                    <div class="col-sm-4 text-right">
                                        <label for="designation" class="control-label form-label-1">Upozilla Zone</label>
                                    </div>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control" name="designation_ref_id" id="select_2" data-placeholder="Select Upozilla">
                                            <option value="">Select Upozilla Zone</option>

                                            <option value='1'>Badda</option>
                                            <option value='1'>Mirpur</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="text" id="" cols="" rows="2" class="form-control" placeholder="Address"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Office Image </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="file" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <h6>BANK INFO</h6>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Bank Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Name of the Bank">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Branch Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Name of the Bank">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Account Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Bank Account Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Account No </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Bank Account No.">
                                    </div>
                                </div>
                            </div>



                            <div class="append_div "></div>
                            <div class="col-12 mb-4">
                                <button id="process_btn" type="button" class="btn btn-info btn-sm custom-btn-1"><i class="fa fa-plus"> Add New Bank</i> </button>

                            </div>



                        </div>
                        <input type="hidden" name="id" value="">
                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    {{--    @include('designation.modal.edit')--}}

    <div id="edit_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update Zone </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>Zone INFO</h6>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Zone Name</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="Username" autocomplete="off" value="" required="" placeholder="Username">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Email </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="email" class="form-control" name="email" value=""  autocomplete="off" required="" placeholder="Email">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Phone </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6">
                                <div class="form-group row select_2_row_modal">
                                    <div class="col-sm-4 text-right">
                                        <label for="designation" class="control-label form-label-1">Upozilla Zone</label>
                                    </div>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control" name="designation_ref_id" id="select_3" data-placeholder="Select Upozilla">
                                            <option value="">Select Upozilla Zone</option>

                                            <option value='1'>Badda</option>
                                            <option value='1'>Mirpur</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="text" id="" cols="" rows="2" class="form-control" placeholder="Address"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Office Image </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="file" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <h6>BANK INFO</h6>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Bank Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Name of the Bank">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Branch Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Name of the Bank">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Account Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Bank Account Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Account No </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="phone" value=""  autocomplete="off" required="" placeholder="Bank Account No.">
                                    </div>
                                </div>
                            </div>



                            <div class="append_div "></div>
                            <div class="col-12 mb-4">
                                <button id="process_btn_2" type="button" class="btn btn-info btn-sm custom-btn-1"><i class="fa fa-plus"> Add New Bank</i> </button>

                            </div>



                        </div>
                        <input type="hidden" name="id" value="">
                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Update</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

@endsection

@section('extra_js')

    <script>

        $(document).ready(function(){

            $("#process_btn").click(function(){
                // $(".append_div").append("<table id='r_table' class='table table-bordered table-responsive'> <thead> <tr> <th>Task Title</th> <th>Description</th> <th>Due Date</th><th>Action</th> </tr> </thead> <tbody> <tr> <td> <div class='form-group'><input type='text' class='form-control' name='task_title[]' autocomplete='off' placeholder='Task Title' required></div></td> <td> <div class='form-group'><textarea class='form-control' name='task_description[]' autocomplete='off' placeholder='Task Description' required></textarea></div></td> <td><input type='date' class='form-control' name='task_due_date[]' autocomplete='off' required></td> <td><button type='button' id='remCF' class='btn btn-danger btn-sm'><i class='fa fa-times'></button></td> </tr></tbody></table>");
                $(".append_div").append("<div class='row row_1_2 pl-3 pr-3'><div class='col-12'><p class='ml-3'> New Bank Info </p></div><div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\"> Bank Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Name of the Bank\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Branch Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Name of the Bank\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Account Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Bank Account Name\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Account No </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Bank Account No.\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div> <div class='col-12 text-right'><button type='button' id='remCF' class='btn btn-danger btn-sm'> x </button></div> </div>");

            });
            $(".append_div").on('click','#remCF',function(){
                $(this).parents(".row_1_2").remove();
            });
            $("#alert_message").fadeTo(1000, 500).slideUp(500, function(){
                $("#alert_message").alert('close');
            });


            $("#process_btn_2").click(function(){
                // $(".append_div").append("<table id='r_table' class='table table-bordered table-responsive'> <thead> <tr> <th>Task Title</th> <th>Description</th> <th>Due Date</th><th>Action</th> </tr> </thead> <tbody> <tr> <td> <div class='form-group'><input type='text' class='form-control' name='task_title[]' autocomplete='off' placeholder='Task Title' required></div></td> <td> <div class='form-group'><textarea class='form-control' name='task_description[]' autocomplete='off' placeholder='Task Description' required></textarea></div></td> <td><input type='date' class='form-control' name='task_due_date[]' autocomplete='off' required></td> <td><button type='button' id='remCF' class='btn btn-danger btn-sm'><i class='fa fa-times'></button></td> </tr></tbody></table>");
                $(".append_div").append("<div class='row row_1_2 pl-3 pr-3'><div class='col-12'><p class='ml-3'> New Bank Info </p></div><div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\"> Bank Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Name of the Bank\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Branch Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Name of the Bank\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Account Name </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Bank Account Name\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"col-sm-6\">\n" +
                    "                                <div class=\"form-group row\">\n" +
                    "                                    <label for=\"form-label\" class=\"col-sm-4 control-label form-label-1\">Account No </label>\n" +
                    "                                    <div class=\"col-sm-8 pl-0\">\n" +
                    "                                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"\"  autocomplete=\"off\" required=\"\" placeholder=\"Bank Account No.\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                            </div> <div class='col-12 text-right'><button type='button' id='remCF' class='btn btn-danger btn-sm'> x </button></div> </div>");

            });


            $('#select_2').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#add_modal .modal-content'),
                placeholder: "Select Upozilla Zone",
            });
            $('#select_3').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#edit_modal .modal-content'),
                placeholder: "Select Upozilla Zone",
            });

            $('#zone_datatable').DataTable({

            });

            //Add
            $("#designation_add_btn").on('click',function (event) {
                event.preventDefault();
                var _token = "{{ csrf_token() }}";
                var designation_name = $("#designation_name").val();
                var remarks = $("#remarks").val();
                //alert(_token);

            });

            // Edit Data
            $(document).on('click','.edit',function () {
                var id=$(this).attr('id');
                var _token="{{csrf_token()}}";
                $("#edit_modal").modal('show');

            });

            // Update
            $("#designation_update_btn").on('click',function (event) {
                event.preventDefault();
                var id=$("#designation_id").val();
                var _token="{{csrf_token()}}";
                var designation_name=$("#designation_name_edit").val();
                var remarks=$("#remarks_edit").val();
                var status=$("#status_edit").val();



            });

            //delete data

            $(document).on('click','.delete',function () {
                var id=$(this).attr('id');
                var _token="{{csrf_token()}}";
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this Designation Data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                        } else {
                            swal("Cancelled", "Your Designation file is safe :)", "error");
                        }
                    });
            });

        })








    </script>

@endsection



