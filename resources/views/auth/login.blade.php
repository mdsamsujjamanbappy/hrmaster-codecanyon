<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="hrMaster: MSBCSE">
    <meta name="author" content="MSBCSE">
    <link rel='shortcut icon' type='image/x-icon' href="{{asset('theme/img/favicon.png')}}">

    <title>Login</title>
    {{ Html::style('theme/lib/@fortawesome/fontawesome-free/css/all.min.css') }}
    {{ Html::style('theme/lib/ionicons/css/ionicons.min.css') }}
    {{ Html::style('theme/css/msb_theme.css') }}
    {{ Html::style('theme/css/login.css') }}


</head>

<body>
<div class="d-flex align-items-center justify-content-center ht-100v">
    <img src="{{asset('theme/img/login_banner.jpg')}}" class="wd-100p ht-100p object-fit-cover" alt="">
    <div class="overlay-body bg-black-6 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 rounded bd bd-white-2 bg-black-7">
            <div class="signin-logo tx-center tx-24 tx-bold tx-white"><span class="tx-normal">[</span> hrMaster <span class="tx-info"></span> <span class="tx-normal">]</span></div>

            <br>
              <h5 class="login-success-message"><?php echo Session::get("successMessage"); ?></h5>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group row">
                    <input id="email" placeholder="Enter Email" type="email" class=" fc-outline-dark form-control @error('email') is-invalid @enderror" name="email" value="admin@email.com" required autocomplete="email" autofocus >

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>

                <div class="form-group row">
                    <input id="password" placeholder="Enter Password" type="password" class=" fc-outline-dark form-control @error('password') is-invalid @enderror" name="password" value="admin" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>

                <div class="form-group row">

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label tx-info" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>

                </div>

                <div class="form-group row mb-0">

                    <button type="submit" class="btn btn-info btn-block">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link tx-info" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                    <span class="login-hints-credential">
                        
                    </span>
                </div>
            </form>

        </div><!-- login-wrapper -->
    </div><!-- overlay-body -->
</div>
{{ Html::script('theme/js/jquery.min.js') }}
{{ Html::script('theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}
{{ Html::script('theme/lib/jquery-ui/ui/widgets/datepicker.js') }}
</body>
</html>