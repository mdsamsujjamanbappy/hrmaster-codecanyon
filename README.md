<h1>hrMaster – Human Resource Management</h1>
<hr>
<img width="450px" src="http://samsujjamanbappy.me/codecanyon_resources/hrmaster/hr_master.jpg" alt="HR MASTER">
<h3>FEATURES:</h3>
<hr>
<ul>
	<li>Multiple Company/Branch Management </li>
	<li>Employee Management </li>
	<li>Attendance Management</li>
	<li>Department Management</li>
	<li>Designation Management</li>
	<li>Multiple Work Shift Management </li>
	<li>Individual Work Shift Management</li>
	<li>Holidays Management</li>
	<li>Leave Types Management </li>
	<li>Leave Application Management </li>
	<li>Leave Application Approval system</li>
	<li>Leave Balance Reporting</li>
	<li>Company’s General Setting</li>
	<li>Employee Profile Setting</li>
	<li>Email Notification Setting</li>
	<li>Award Management</li>
	<li>Notice Management</li>
	<li>Expense Management</li>
	<li>Attendance Report</li>
	<li>Multiple Access Role Control Management </li>
	<li>So on..</li>
</ul>
<br>
<h3>Demo Login Access</h3>
<hr>
<p>Login URL:  <b>https://samsujjamanbappy.me/hrmaster</b>
<br>
<br>
<b>Admin Panel</b>
<br>
Email: <b>admin@email.com</b>
<br>
Password: <b>admin</b>
<br><br>
<b>Employee Panel</b>
<br>
Email: <b>employee@email.com</b>
<br>
Password: <b>123456</b>
</p>
<br>
<h3>Server Requirements</h3>
<hr>
<ul>
	<li>PHP &gt;= 7.2.5</li>
	<li>BCMath PHP Extension</li>
	<li>Ctype PHP Extension</li>
	<li>Fileinfo PHP extension</li>
	<li>JSON PHP Extension</li>
	<li>Mbstring PHP Extension</li>
	<li>OpenSSL PHP Extension</li>
	<li>PDO PHP Extension</li>
	<li>Tokenizer PHP Extension</li>
	<li>XML PHP Extension</li>
</ul>
<br>
<p>Let me know if you have any questions or queries via <a href="mailto:samsujjamanbappy@gmail.com">samsujjamanbappy@gmail.com</a> </p>