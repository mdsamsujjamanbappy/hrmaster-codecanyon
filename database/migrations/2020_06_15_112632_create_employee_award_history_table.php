<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAwardHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_award_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employee_id')->comment('employee_id will come from ("tb_employee_list") table')->nullable();
            $table->string('award_name', 200)->nullable();
            $table->string('award_gift', 200)->nullable();
            $table->string('cash_price', 200)->nullable();
            $table->date('award_period')->nullable();
            $table->text('remarks')->nullable();
            $table->text('award_attachment')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->tinyInteger('status')->comment('"1" is enable or  "0" disable')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_award_history');
    }
}
