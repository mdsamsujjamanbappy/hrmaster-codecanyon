<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeaveTypeSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_leave_type_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('company_id')->nullable();
            $table->string('leave_type_name', 100)->nullable();
            $table->integer('total_leave_days')->nullable();
            $table->text('remarks')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->tinyInteger('status')->comment('"1" for active,  "0" for inactive')->default()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_leave_type_setting');
    }
}
