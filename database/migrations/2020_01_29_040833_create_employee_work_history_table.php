<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeWorkHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_work_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('emp_id')->comment('emp_id will come from ("tb_employee_list.id") table')->nullable()->index();
            $table->string('wh_company_name', 200)->nullable();
            $table->string('wh_designation', 150)->nullable();
            $table->date('wh_joining_date')->nullable();
            $table->date('wh_resign_date')->nullable();
            $table->text('wh_description')->nullable();
            $table->text('wh_attachment')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_work_history');
    }
}
