<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MerchandisingBuyerListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_merchandising_buyer_list')->insert([
            [  
                'id'            => 1,
                'buyer_name' 	=> "Vershold Poland Sp. z o. o.",
                'buyer_phone'   => "1234567890",
                'buyer_email'   => "anna.salacinska@vershold.com",
                'buyer_address' => "Zwirki i Wigury 16A, 02-092 Warszawa, Mazowieckie, Poland",
                'buyer_remarks' => "None",
                'created_by'   	=> 1,
                'status'        => 1,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString()
            ]
        ]);
    }
}
