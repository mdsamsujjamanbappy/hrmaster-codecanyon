<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CompanyListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_company_information')->insert([
            [  
                'id'                => 1,
                'company_logo'      => "default.png",
                'company_name'      => "Daylight Textile Limited.",
                'company_tagline'   => 'Safe Textile Solution',
                'company_phone'     => '55087469',
                'company_email'     => 'info@daylight.com',
                'company_address1'  => 'Sonargaon Janapath, Uttara 11, Dhaka 1230',
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 2,
                'company_logo'      => "default.png",
                'company_name'      => "Dongyi Sourcing Limited.",
                'company_tagline'   => 'For Safe Sourcing',
                'company_phone'     => '55087511',
                'company_email'     => 'info@dongyi-bd.com',
                'company_address1'  => 'Sonargaon Janapath, Uttara 11, Dhaka 1230',
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 3,
                'company_logo'      => "default.png",
                'company_name'      => "Southfield Knitwear.",
                'company_tagline'   => 'For Quality Knitwear',
                'company_phone'     => '55087510',
                'company_email'     => 'info@southfieldknit.com',
                'company_address1'  => 'Sonargaon Janapath, Uttara 11, Dhaka 1230',
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}
