<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MerchandisingBankListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_merchandising_bank_list')->insert([
            [  
                'id'                	=> 1,
                'bank_name'   			=> "United Commercial Bank Ltd.",
                'bank_address'      	=> "Sonargaon Janapath Branch, House: 10, Sector: 11, Uttara, Dhaka.",
                'bank_phone'      		=> "8991584",
                'bank_fax'      		=> NULL,
                'bank_email'      		=> "sng@ucb.com.bd",
                'bank_swift_code'      	=> "UCBLBDDH",
                'bank_account_name'     => "Dongyi Sourcing Limited",
                'bank_account_number'   => "1052101000009305",
                'created_by'      		=> 1,
                'status'            	=> 1,
                'created_at'        	=> Carbon::now()->toDateTimeString(),
                'updated_at'        	=> Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                	=> 2,
                'bank_name'   			=> "NRBC Bank",
                'bank_address'      	=> "Uttara Branch, Masum Plaza, (Ground & 2nd Floor), House: 13, Road:15, Rabindra Sharani, Sector: 3, Uttara, Dhaka – 1230.",
                'bank_phone'      		=> "58950192",
                'bank_fax'      		=> "58950126",
                'bank_email'      		=> "uttara@nrbcommercialbank.com",
                'bank_swift_code'      	=> "NRBBBDDHUTR",
                'bank_account_name'     => "Dongyi Sourcing Limited",
                'bank_account_number'   => "010833300000866",
                'created_by'      		=> 1,
                'status'            	=> 1,
                'created_at'        	=> Carbon::now()->toDateTimeString(),
                'updated_at'        	=> Carbon::now()->toDateTimeString()
            ]
        ]);
    }
}
