<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('users')->insert([
            [  'id'             => 1,
                'user_type'     => 1,
                'name'          => 'super admin',
                'email'         => 'super@email.com',
                'password'      => bcrypt('admin'),
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'status'        => 1,
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ],

            [   'id'           => 2,
                'user_type'    => 2,
                'name'         => 'admin',
                'email'        => 'admin@email.com',
                'password'     => bcrypt('admin'),
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'status'        => 1,
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}
