<?php
function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->user_type);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {

            case 1:
            return 'super_admin';
            break;

            case 2:
            return 'group_admin';
            break;

            case 3:
            return 'group_director';
            break;

            case 4:
            return 'company_director';
            break;

            case 5:
            return 'company_admin';
            break;

            case 7:
            return 'company_admin'; //company_hr will configure later
            break;

            case 12:
            return 'employee';
            break;
    }
}

