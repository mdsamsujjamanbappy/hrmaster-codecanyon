<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EmployeeAttendanceController extends Controller
{

    public function give_attendance_by_app()
    {
        return view("attendance_management.give_attendance_by_app");
    }

    public function give_attendance_by_app_checkin(Request $request)
    {
        $attendance_date =  date('Y-m-d');
        $check_time = date('H:i:s');

        $emp_ref_id = Auth::user()->ref_id;
        $employee_id = DB::table("tb_employee_list")->select('employee_id')->where('tb_employee_list.id', '=', $emp_ref_id)->first();
        $check = DB::table("tb_attendance_history")->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $attendance_date]])->count();

        if($check==0){
            $str = DB::table('tb_attendance_history')->insertGetId([
                'emp_id'            =>  $employee_id->employee_id,
                'attendance_date'   =>  $attendance_date,
                'in_time'           =>  $check_time,
                'attendance_type'   =>  2,
                'created_at'        =>  Carbon::now()->toDateTimeString(),
                'updated_at'        =>  Carbon::now()->toDateTimeString()
            ]);

            $str = DB::table('tb_app_attendance_history')->insert([
                'emp_id'            =>  $employee_id->employee_id,
                'attendance_ref_id' =>  $str,
                'attendance_date'   =>  $attendance_date,
                'check_in'          =>  $check_time,
                'check_in_lat'      =>  $request->check_in_lat,
                'check_in_long'     =>  $request->check_in_long,
                'check_in_ip'       =>  $request->check_in_ip,
                'created_at'        =>  Carbon::now()->toDateTimeString(),
                'updated_at'        =>  Carbon::now()->toDateTimeString()
            ]);
        
        Session::flash('successMessage','Attendance has been successfully recorded.');
        }else{
            Session::flash('failedMessage','Attendance data has been already exists.');
        }
        

        return redirect()->back();
    }

    public function give_attendance_by_app_checkout(Request $request)
    {
        $attendance_date =  date('Y-m-d');
        $check_time = date('H:i:s');

        $emp_ref_id = Auth::user()->ref_id;
        $employee_id = DB::table("tb_employee_list")->select('employee_id')->where('tb_employee_list.id', '=', $emp_ref_id)->first();
        $check = DB::table("tb_attendance_history")->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $attendance_date]])->count();

        if($check!=0){
            $str = DB::table('tb_attendance_history')->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $attendance_date]])->update([
                'out_time'          =>  $check_time,
                'updated_at'        =>  Carbon::now()->toDateTimeString()
                ]);

            $str = DB::table('tb_app_attendance_history')->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $attendance_date]])->update([
                'check_out'          =>  $check_time,
                'check_out_lat'      =>  $request->check_out_lat,
                'check_out_long'     =>  $request->check_out_long,
                'check_out_ip'       =>  $request->check_out_ip,
                'created_at'        =>  Carbon::now()->toDateTimeString(),
                'updated_at'        =>  Carbon::now()->toDateTimeString()
            ]);
        
        Session::flash('successMessage','Check out time has been successfully updated.');
        }else{
            Session::flash('failedMessage','Please checkin First..');
        }
        

        return redirect()->back();
    }

    public function todays_attendance()
    {
        // if (request()->ajax()) {
            $today = date("Y-m-d");

            //Office Time default
            $office_time_default = DB::table('tb_work_shift_list')->orderBy('id')->first();
            //Office Time default

            //Employee List
            $employeeLists = DB::table("tb_employee_list")
                ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
                ->select(
                    "tb_employee_list.id as employee_primary_id",
                    "tb_employee_list.emp_first_name",
                    "tb_employee_list.emp_last_name",
                    "tb_employee_list.employee_id",
                    'tb_work_shift_list.id as shift_number',
                    'tb_work_shift_list.shift_name',
                    'tb_work_shift_list.entry_time as start_time',
                    'tb_work_shift_list.exit_time as end_time',
                    'tb_work_shift_list.buffer_time'
                )->orderBy('tb_employee_list.employee_id', 'asc')
                ->get();
            //Employee List

            $attendance_data = array();
            $day = date('l', strtotime($today));

            $office_start_time = "";
            $office_end_time = "";
            $office_buffer_time = "";
            // dd($employeeLists);
            foreach ($employeeLists as $employeeList) {
                
                if(!empty($employeeList->shift_number)){
                     //Weekend
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                        ->where('shift_id', '=', $employeeList->shift_number)
                        ->get()->toArray());

                    $weekend = array_column($weekend, 'day_name');
                    //Weekend
                }else{
                    //Weekend
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                        ->where('shift_id', '=', $office_time_default->id)
                        ->get()->toArray());

                    $weekend = array_column($weekend, 'day_name');
                    //Weekend
                }

                // Office Start Time
                if (!empty($employeeList->start_time)) {
                    $office_start_time = $employeeList->start_time;
                } else {
                    $office_start_time = $office_time_default->entry_time;
                }

                // Office End Time
                if (!empty($employeeList->end_time)) {
                    $office_end_time = $employeeList->end_time;
                } else {
                    $office_end_time = $office_time_default->exit_time;
                }

                // Office Buffer Time
                if (!empty($employeeList->buffer_time)) {
                    $office_buffer_time = $employeeList->buffer_time;
                } else {
                    $office_buffer_time = $office_time_default->buffer_time;
                }

                //Employee attendance
                $attendances = DB::table('tb_attendance_history')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->select(
                        'tb_employee_list.id',
                        'tb_employee_list.employee_id',
                        'tb_employee_list.emp_first_name',
                        'tb_employee_list.emp_last_name',
                        'tb_attendance_history.attendance_date as attendance_date',
                        'tb_attendance_history.in_time as inTime',
                        'tb_attendance_history.out_time as outTime'
                    )
                    ->join('tb_employee_list', 'tb_attendance_history.emp_id', 'tb_employee_list.employee_id')
                    ->where('tb_attendance_history.attendance_date', 'Like', "%$today%")
                    ->where('tb_attendance_history.emp_id', '=', $employeeList->employee_id)
                    ->first();
                //Employee attendance

                //Present Status
                $status = '';
                if (in_array($day, $weekend) && !empty($attendances->inTime)) {
                    $status = 'Present-Weekend';
                } elseif (in_array($day, $weekend)) {
                    $status = 'Weekend';
                } elseif (!empty($attendances->inTime)) {
                    $status = 'Present';
                } else {
                    $leaveLists = '';
                    $status = 'Absent';
                    $leaveLists = DB::table('tb_employee_leave_application')
                        ->where('employee_id', '=', $employeeList->employee_primary_id)
                        ->whereYear('leave_starting_date', Carbon::now()->year)
                        ->whereMonth('leave_starting_date', Carbon::now()->month)
                        ->where('status', '=', 1)->get();

                    if (!empty($leaveLists)) {
                        foreach ($leaveLists as $leaveList) {
                            if ($leaveList->leave_starting_date <= $today &&  $leaveList->leave_ending_date >= $today) {
                                $status = 'Leave';
                            } else {
                                $status = 'Absent';
                            }
                        }
                    } else {
                        $status = 'Absent';
                    }
                }

                // Present Status
                // Late Calculation
                $late = '';
                if (!empty($attendances->inTime)) {

                    $office_start_late = strtotime($office_start_time);

                    $attendance_in_time_late = strtotime($attendances->inTime);

                    if ($office_start_late < $attendance_in_time_late) {

                        $buffer_time = gmdate('H:i', strtotime($office_buffer_time) - strtotime($office_start_time));

                        $late = gmdate('H:i', $attendance_in_time_late - $office_start_late);

                        if ($late > $buffer_time) {
                            $late = $late;
                        } else {
                            $late = '';
                        }
                    }
                }

                // Late Calculation
                // Working Hour Calculation
                $work_hour = "";
                if (!empty($attendances->inTime) && !empty($attendances->outTime)) {
                    $work_hour = gmdate('H:i', strtotime($attendances->outTime) - strtotime($attendances->inTime));
                }

                //Working Hour Calculation
                //Over Time Calculation
                $over_time = '';
                $total_work_hour = '';
                if ($work_hour != '') {
                    $total_work_hour = gmdate('H:i', strtotime($office_end_time) - strtotime($office_start_time));
                    if (strtotime($work_hour) > strtotime($total_work_hour)) {
                        $over_time = gmdate('H:i', strtotime($work_hour) - strtotime($total_work_hour));
                    }
                }

                // Over Time Calculation

                // Early Deparature Calculation
                $early_deparature = '';
                if (!empty($attendances->outTime)) {
                    if ($attendances->outTime < $office_end_time) {
                        $early_deparature = gmdate('H:i', strtotime($office_end_time) - strtotime($attendances->outTime));
                    }
                }

                //Early Deparature Calculation
                if(!empty($late)){
                    $status="Late";
                }
                $attendance_data[] = [
                    'entry_date' => $attendances->date ?? date('d-M-Y', strtotime($today)),
                    'day' => $day ?? '',
                    'employee_id' => $employeeList->employee_id,
                    'employee_name' => $employeeList->emp_first_name." ".$employeeList->emp_last_name,
                    'shift' => $employeeList->shift_name ?? $office_time_default->shift_name,
                    'start_time' => $office_start_time,
                    'in_time' => $attendances->inTime ?? '',
                    'late' => $late ?? '',
                    'end_time' => $office_end_time,
                    'out_time' => $attendances->outTime ?? '',
                    'early_deparature' => $early_deparature ?? '',
                    'total_work_hour' => $total_work_hour ?? "",
                    'work_hour' => $work_hour ?? "",
                    'over_time' => $over_time ?? "",
                    'status' => $status
                ];
            // }
            // return datatables()
            //     ->of($data)
            //     ->addIndexColumn()
            //     ->make(true);
        };
        // dd($attendance_data);
        return view("attendance_management.employee_today_attendance", compact('attendance_data'));
    }


    public function daily_attendance_view()
    {
        return view("attendance_management.employee_daily_attendance_view");
    }

    public function daily_attendance(Request $request)
    {
        // if (request()->ajax()) {
            $today = $request->attendance_date;

            //        Office Time default
            $office_time_default = DB::table('tb_work_shift_list')->orderBy('id')->first();
            //        Office Time default

            $holidays=array();
            $holidayLists = DB::table('tb_holidays_observances_leave')
                ->whereYear('tb_holidays_observances_leave.start_date', Carbon::now()->year)
                ->whereMonth('tb_holidays_observances_leave.start_date', Carbon::now()->month)
                ->where('status', '=', 1)->get()->toArray();
            foreach ($holidayLists as $list) {
                $holiday = $this->getDatesFromRange($list->start_date, $list->end_date);
                foreach ($holiday as $key) {
                    array_push($holidays, $key);
                }
            }
            //        employee List
            $employeeLists = DB::table("tb_employee_list")
                ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
                ->select(
                    "tb_employee_list.id as employee_primary_id",
                    "tb_employee_list.emp_first_name",
                    "tb_employee_list.emp_last_name",
                    "tb_employee_list.employee_id",
                    'tb_work_shift_list.id as shift_number',
                    'tb_work_shift_list.shift_name',
                    'tb_work_shift_list.entry_time as start_time',
                    'tb_work_shift_list.exit_time as end_time',
                    'tb_work_shift_list.buffer_time'
                )->get();
            //        employee List

            $attendance_data = array();
            $day = date('l', strtotime($today));

            $office_start_time = "";
            $office_end_time = "";
            $office_buffer_time = "";

            foreach ($employeeLists as $employeeList) {
                 
                if(!empty($employeeList->shift_number)){
                     //Weekend
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                        ->where('shift_id', '=', $employeeList->shift_number)
                        ->get()->toArray());

                    $weekend = array_column($weekend, 'day_name');
                    //Weekend
                }else{
                    //Weekend
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                        ->where('shift_id', '=', $office_time_default->id)
                        ->get()->toArray());

                    $weekend = array_column($weekend, 'day_name');
                    //Weekend
                }

                //                Office Start Time
                if (!empty($employeeList->start_time)) {
                    $office_start_time = $employeeList->start_time;
                } else {
                    $office_start_time = $office_time_default->entry_time;
                }

                //                Office End Time
                if (!empty($employeeList->end_time)) {
                    $office_end_time = $employeeList->end_time;
                } else {
                    $office_end_time = $office_time_default->exit_time;
                }

                //                Office Buffer Time
                if (!empty($employeeList->buffer_time)) {
                    $office_buffer_time = $employeeList->buffer_time;
                } else {
                    $office_buffer_time = $office_time_default->buffer_time;
                }

                //Employee attendance
                $attendances = DB::table('tb_attendance_history')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->select(
                        'tb_employee_list.id',
                        'tb_employee_list.employee_id',
                        'tb_employee_list.emp_first_name',
                        'tb_employee_list.emp_last_name',
                        'tb_attendance_history.attendance_date as attendance_date',
                        'tb_attendance_history.in_time as inTime',
                        'tb_attendance_history.out_time as outTime'
                    )
                    ->join('tb_employee_list', 'tb_attendance_history.emp_id', 'tb_employee_list.employee_id')
                    ->where('tb_attendance_history.attendance_date', 'Like', "%$today%")
                    ->where('tb_attendance_history.emp_id', '=', $employeeList->employee_id)
                    ->first();

                //Employee attendance
                
                // dd($attendances);

                //Present Status
                $status = '';
                if (in_array($today, $holidays) && !empty($attendances->inTime)) {
                    $status = 'Present-Holiday';
                }elseif (in_array($today, $holidays)) {
                    $status = 'Holiday';
                }elseif (in_array($day, $weekend) && !empty($attendances->inTime)) {
                    $status = 'Present-Weekend';
                } elseif (in_array($day, $weekend)) {
                    $status = 'Weekend';
                } elseif (!empty($attendances->inTime)) {
                    $status = 'Present';
                } else {
                    $leaveLists = '';
                    $status = 'Absent';
                    $leaveLists = DB::table('tb_employee_leave_application')
                        ->where('employee_id', '=', $employeeList->employee_primary_id)
                        ->whereYear('leave_starting_date', Carbon::now()->year)
                        ->where('status', '=', 1)->get();

                    if (!empty($leaveLists)) {
                        foreach ($leaveLists as $leaveList) {
                            if ($leaveList->leave_starting_date <= $today &&  $leaveList->leave_ending_date >= $today) {
                                $status = 'Leave';
                            } else {
                                $status = 'Absent';
                            }
                        }
                    } else {
                        $status = 'Absent';
                    }
                }

                //                Present Status

                //                Late Calculation
                $late = '';
                if (!empty($attendances->inTime)) {

                    $office_start_late = strtotime($office_start_time);

                    $attendance_in_time_late = strtotime($attendances->inTime);

                    if ($office_start_late < $attendance_in_time_late) {

                        $buffer_time = gmdate('H:i', strtotime($office_buffer_time) - strtotime($office_start_time));

                        $late = gmdate('H:i', $attendance_in_time_late - $office_start_late);

                        if ($late > $buffer_time) {
                            $late = $late;
                        } else {
                            $late = '';
                        }
                    }
                }
                //                Late Calculation

                //                Working Hour Calculation
                $work_hour = "";
                if (!empty($attendances->inTime) && !empty($attendances->outTime)) {
                    $work_hour = gmdate('H:i', strtotime($attendances->outTime) - strtotime($attendances->inTime));
                }
                //                Working Hour Calculation

                //                Over Time Calculation
                $over_time = '';
                $total_work_hour = '';
                if ($work_hour != '') {
                    $total_work_hour = gmdate('H:i', strtotime($office_end_time) - strtotime($office_start_time));
                    if (strtotime($work_hour) > strtotime($total_work_hour)) {
                        $over_time = gmdate('H:i', strtotime($work_hour) - strtotime($total_work_hour));
                    }
                }
                //                Over Time Calculation

                //                early Deparature Calculation
                $early_deparature = '';
                if (!empty($attendances->outTime)) {
                    if ($attendances->outTime < $office_end_time) {
                        $early_deparature = gmdate('H:i', strtotime($office_end_time) - strtotime($attendances->outTime));
                    }
                }
                //                early Deparature Calculation
                if(!empty($late)){
                    $status="Late";
                }
                $attendance_data[] = [
                    'entry_date' => $attendances->date ?? date('d-M-Y', strtotime($today)),
                    'day' => $day ?? '',
                    'employee_id' => $employeeList->employee_id,
                    'employee_name' => $employeeList->emp_first_name." ".$employeeList->emp_last_name,
                    'shift' => $employeeList->shift_name ?? $office_time_default->shift_name,
                    'start_time' => $office_start_time,
                    'in_time' => $attendances->inTime ?? '',
                    'late' => $late ?? '',
                    'end_time' => $office_end_time,
                    'out_time' => $attendances->outTime ?? '',
                    'early_deparature' => $early_deparature ?? '',
                    'total_work_hour' => $total_work_hour ?? "",
                    'work_hour' => $work_hour ?? "",
                    'over_time' => $over_time ?? "",
                    'status' => $status
                ];
            // }
            // return datatables()->of($data)
            //     ->addIndexColumn()
            //     ->make(true);
        };
        return view("attendance_management.employee_daily_attendance", compact('attendance_data', 'request'));
    }

    public function date_wise_attendance_view(){
        $user_company = Auth::user()->company_id;
        
        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
            $employee_list = DB::table('tb_employee_list')
            ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name')
            ->orderBy('tb_employee_list.employee_id', 'asc')
            ->where('emp_account_status', '=', 1)
            ->get();
        }else{
            $employee_list = DB::table('tb_employee_list')
            ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name')
            ->orderBy('tb_employee_list.employee_id', 'asc')
            ->where([['emp_account_status', '=', 1], ['company_id', '=', $user_company]])
            ->get();
        }
        
        return view("attendance_management.date_wise_attendance_view", compact('employee_list'));
    }

    public function date_wise_attendance_data(Request $request)
    {
            $search_employee = '';
            if ($request->employee_id == 'all') {

                if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){

                    //Employee List
                    $employeeLists = DB::table("tb_employee_list")
                        ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
                        ->select(
                            "tb_employee_list.id as employee_primary_id",
                            "tb_employee_list.emp_first_name",
                            "tb_employee_list.emp_last_name",
                            "tb_employee_list.employee_id",
                            'tb_work_shift_list.id as shift_number',
                            'tb_work_shift_list.shift_name',
                            'tb_work_shift_list.entry_time as start_time',
                            'tb_work_shift_list.exit_time as end_time',
                            'tb_work_shift_list.buffer_time'
                        )->orderBy('tb_employee_list.employee_id', 'asc')
                        ->where('emp_account_status', '=', 1)
                        ->get();
                    //Employee List

                }else{

                    $user_company = Auth::user()->company_id;
                    //Employee List
                    $employeeLists = DB::table("tb_employee_list")
                        ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
                        ->select(
                            "tb_employee_list.id as employee_primary_id",
                            "tb_employee_list.emp_first_name",
                            "tb_employee_list.emp_last_name",
                            "tb_employee_list.employee_id",
                            'tb_work_shift_list.id as shift_number',
                            'tb_work_shift_list.shift_name',
                            'tb_work_shift_list.entry_time as start_time',
                            'tb_work_shift_list.exit_time as end_time',
                            'tb_work_shift_list.buffer_time'
                        )->orderBy('tb_employee_list.employee_id', 'asc')
                        ->where([['emp_account_status', '=', 1], ['company_id', '=', $user_company]])
                        ->get();
                    //Employee List
                }

            } else {
                $search_employee = $request->employee_id;

                $employeeLists = DB::table("tb_employee_list")
                    ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
                    ->select(
                        "tb_employee_list.id as employee_primary_id",
                        "tb_employee_list.emp_first_name",
                        "tb_employee_list.emp_last_name",
                        "tb_employee_list.employee_id",
                        'tb_work_shift_list.id as shift_number',
                        'tb_work_shift_list.shift_name',
                        'tb_work_shift_list.entry_time as start_time',
                        'tb_work_shift_list.exit_time as end_time',
                        'tb_work_shift_list.buffer_time'
                    )->orderBy('tb_employee_list.employee_id', 'asc')
                    ->where([['emp_account_status', '=', 1], ['tb_employee_list.id', '=', $search_employee]])
                    ->get();
            }

            $search_from =  $request->search_from;
            $search_to = $request->search_to;

            $holidays=array();
            $holidayLists = DB::table('tb_holidays_observances_leave')
                ->whereYear('tb_holidays_observances_leave.start_date', Carbon::now()->year)
                ->whereMonth('tb_holidays_observances_leave.start_date', Carbon::now()->month)
                ->where('status', '=', 1)->get()->toArray();
            foreach ($holidayLists as $list) {
                $holiday = $this->getDatesFromRange($list->start_date, $list->end_date);
                foreach ($holiday as $key) {
                    array_push($holidays, $key);
                }
            }

            //Default Office Time 
            $office_time_default = DB::table('tb_work_shift_list')->orderBy('id')->first();
            //Default Office Time

            // Input Data Resorting
            if (!empty($search_from)) {
                $min = date("Y-m-d", strtotime($search_from));
            } else {
                $min = date("Y-m-01");
            }

            if (!empty($search_to)) {
                $maxdate = date("Y-m-d", strtotime($search_to));
            } else {
                $maxdate = date("Y-m-d");
            }
            // Input Data Resorting

            $attendance_data = array();
            while ($maxdate >= $min) {

            $day = date('l', strtotime($maxdate));
            $today = date('Y-m-d', strtotime($maxdate));

            $office_start_time = "";
            $office_end_time = "";
            $office_buffer_time = "";

            foreach ($employeeLists as $employeeList) {

                //Weekend
                if(!empty($employeeList->shift_number)){
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                    ->where('shift_id', '=', $employeeList->shift_number)
                    ->get()->toArray());
                }else{
                    $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                    ->where('shift_id', '=', $office_time_default->id)
                    ->get()->toArray());
                }
                  
                // Office Start Time
                if (!empty($employeeList->start_time)) {
                    $office_start_time = $employeeList->start_time;
                } else {
                    $office_start_time = $office_time_default->entry_time;
                }

                // Office End Time
                if (!empty($employeeList->end_time)) {
                    $office_end_time = $employeeList->end_time;
                } else {
                    $office_end_time = $office_time_default->exit_time;
                }

                // Office Buffer Time
                if (!empty($employeeList->buffer_time)) {
                    $office_buffer_time = $employeeList->buffer_time;
                } else {
                    $office_buffer_time = $office_time_default->buffer_time;
                }

                //Employee attendance
                $attendances = DB::table('tb_attendance_history')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->select(
                        'tb_employee_list.id',
                        'tb_employee_list.employee_id',
                        'tb_employee_list.emp_first_name',
                        'tb_employee_list.emp_last_name',
                        'tb_attendance_history.attendance_date as attendance_date',
                        'tb_attendance_history.in_time as inTime',
                        'tb_attendance_history.out_time as outTime',
                        'tb_attendance_history.attendance_type as attendance_type'
                    )
                    ->join('tb_employee_list', 'tb_attendance_history.emp_id', 'tb_employee_list.employee_id')
                    ->where('tb_attendance_history.attendance_date', 'Like', "%$maxdate%")
                    ->where('tb_attendance_history.emp_id', '=', $employeeList->employee_id)
                    ->first();

                //Employee attendance

                //Present Status
                $status = '';
                if (in_array($today, $holidays) && !empty($attendances->inTime)) {
                    $status = 'Present-Holiday';
                }elseif (in_array($today, $holidays)) {
                    $status = 'Holiday';
                }elseif (in_array($day, $weekend) && !empty($attendances->inTime)) {
                    $status = 'Present-Weekend';
                } elseif (in_array($day, $weekend)) {
                    $status = 'Weekend';
                } elseif (!empty($attendances->inTime)) {
                    $status = 'Present';
                } else {
                    $leaveLists = '';
                    $status = 'Absent';
                    $leaveLists = DB::table('tb_employee_leave_application')
                        ->where('employee_id', '=', $employeeList->employee_primary_id)
                        ->whereYear('leave_starting_date', Carbon::now()->year)
                        ->where('status', '=', 1)->get();

                    if (!empty($leaveLists)) {
                        foreach ($leaveLists as $leaveList) {
                            if ($leaveList->leave_starting_date <= $today &&  $leaveList->leave_ending_date >= $today) {
                                $status = 'Leave';
                            } else {
                                $status = 'Absent';
                            }
                        }
                    } else {
                        $status = 'Absent';
                    }
                }

                // Present Status
                // Late Calculation
                $late = '';
                if (!empty($attendances->inTime)) {

                    $office_start_late = strtotime($office_start_time);

                    $attendance_in_time_late = strtotime($attendances->inTime);

                    if ($office_start_late < $attendance_in_time_late) {

                        $buffer_time = gmdate('H:i', strtotime($office_buffer_time) - strtotime($office_start_time));

                        $late = gmdate('H:i', $attendance_in_time_late - $office_start_late);

                        if ($late > $buffer_time) {
                            $late = $late;
                        } else {
                            $late = '';
                        }
                    }
                }

                // Late Calculation
                // Working Hour Calculation
                $work_hour = "";
                if (!empty($attendances->inTime) && !empty($attendances->outTime)) {
                    $work_hour = gmdate('H:i', strtotime($attendances->outTime) - strtotime($attendances->inTime));
                }

                //Working Hour Calculation
                //Over Time Calculation
                $over_time = '';
                $total_work_hour = '';
                if ($work_hour != '') {
                    $total_work_hour = gmdate('H:i', strtotime($office_end_time) - strtotime($office_start_time));
                    if (strtotime($work_hour) > strtotime($total_work_hour)) {
                        $over_time = gmdate('H:i', strtotime($work_hour) - strtotime($total_work_hour));
                    }
                }

                // Over Time Calculation

                // Early Deparature Calculation
                $early_deparature = '';
                if (!empty($attendances->outTime)) {
                    if ($attendances->outTime < $office_end_time) {
                        $early_deparature = gmdate('H:i', strtotime($office_end_time) - strtotime($attendances->outTime));
                    }
                }

                //Early Deparature Calculation
                if(!empty($late)){
                    $status="Late";
                }
                $attendance_data[] = [
                    'entry_date' => $attendances->date ?? date('d-M-Y', strtotime($maxdate)),
                    'day' => $day ?? '',
                    'employee_id' => $employeeList->employee_id,
                    'employee_name' => $employeeList->emp_first_name." ".$employeeList->emp_last_name,
                    'shift' => $employeeList->shift_name ?? $office_time_default->shift_name,
                    'start_time' => $office_start_time,
                    'in_time' => $attendances->inTime ?? '',
                    'attendance_type' => $attendances->attendance_type ?? '',
                    'late' => $late ?? '',
                    'end_time' => $office_end_time,
                    'out_time' => $attendances->outTime ?? '',
                    'early_deparature' => $early_deparature ?? '',
                    'total_work_hour' => $total_work_hour ?? "",
                    'work_hour' => $work_hour ?? "",
                    'over_time' => $over_time ?? "",
                    'status' => $status
                ];
                }

                $maxdate = date('Y-m-d', (strtotime('-1 day', strtotime($maxdate))));

        };

        return view("attendance_management.date_wise_attendance_data", compact('attendance_data', 'request'));
    }

    public function myself_date_wise_attendance_view(){
        return view("attendance_management.myself_date_wise_attendance_view");
    }

    public function myself_date_wise_attendance_data(Request $request)
    {
        $search_employee = auth()->user()->ref_id;

        $employeeLists = DB::table("tb_employee_list")
            ->leftJoin('tb_work_shift_list', 'tb_employee_list.emp_shift_id', 'tb_work_shift_list.id')
            ->select(
                "tb_employee_list.id as employee_primary_id",
                "tb_employee_list.emp_first_name",
                "tb_employee_list.emp_last_name",
                "tb_employee_list.employee_id",
                'tb_work_shift_list.id as shift_number',
                'tb_work_shift_list.shift_name',
                'tb_work_shift_list.entry_time as start_time',
                'tb_work_shift_list.exit_time as end_time',
                'tb_work_shift_list.buffer_time'
            )->orderBy('tb_employee_list.employee_id', 'asc')
            ->where([['emp_account_status', '=', 1], ['tb_employee_list.id', '=', $search_employee]])
            ->get();

            $search_from =  $request->search_from;
            $search_to = $request->search_to;

            $holidays=array();
            $holidayLists = DB::table('tb_holidays_observances_leave')
                ->whereYear('tb_holidays_observances_leave.start_date', Carbon::now()->year)
                ->whereMonth('tb_holidays_observances_leave.start_date', Carbon::now()->month)
                ->where('status', '=', 1)->get()->toArray();
            foreach ($holidayLists as $list) {
                $holiday = $this->getDatesFromRange($list->start_date, $list->end_date);
                foreach ($holiday as $key) {
                    array_push($holidays, $key);
                }
            }

            //Default Office Time 
            $office_time_default = DB::table('tb_work_shift_list')->orderBy('id')->first();
            //Default Office Time

           $emp_shift_id = DB::table("tb_employee_list")->where([['emp_account_status', '=', 1], ['tb_employee_list.id', '=', $search_employee]])->first('emp_shift_id');

           if(!empty($emp_shift_id->emp_shift_id)){
                //Weekend
                $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                    ->where('shift_id', '=', $emp_shift_id->emp_shift_id)
                    ->get()->toArray());

                $weekend = array_column($weekend, 'day_name');
                //Weekend
            }else{
                //Weekend
                $weekend = array_values(DB::table('tb_employee_shift_weekend')->select("day_name")
                    ->where('shift_id', '=', $office_time_default->id)
                    ->get()->toArray());

                $weekend = array_column($weekend, 'day_name');
                //Weekend
            }

            // Input Data Resorting
            if (!empty($search_from)) {
                $min = date("Y-m-d", strtotime($search_from));
            } else {
                $min = date("Y-m-01");
            }

            if (!empty($search_to)) {
                $maxdate = date("Y-m-d", strtotime($search_to));
            } else {
                $maxdate = date("Y-m-d");
            }
            // Input Data Resorting

            $attendance_data = array();
            while ($maxdate >= $min) {

               $day = date('l', strtotime($maxdate));
               $today = date('Y-m-d', strtotime($maxdate));

            $office_start_time = "";
            $office_end_time = "";
            $office_buffer_time = "";

            foreach ($employeeLists as $employeeList) {
                // Office Start Time
                if (!empty($employeeList->start_time)) {
                    $office_start_time = $employeeList->start_time;
                } else {
                    $office_start_time = $office_time_default->entry_time;
                }

                // Office End Time
                if (!empty($employeeList->end_time)) {
                    $office_end_time = $employeeList->end_time;
                } else {
                    $office_end_time = $office_time_default->exit_time;
                }

                // Office Buffer Time
                if (!empty($employeeList->buffer_time)) {
                    $office_buffer_time = $employeeList->buffer_time;
                } else {
                    $office_buffer_time = $office_time_default->buffer_time;
                }

                //Employee attendance
                $attendances = DB::table('tb_attendance_history')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->orderBy('tb_attendance_history.in_time', 'desc')
                    ->select(
                        'tb_employee_list.id',
                        'tb_employee_list.employee_id',
                        'tb_employee_list.emp_first_name',
                        'tb_employee_list.emp_last_name',
                        'tb_attendance_history.attendance_date as attendance_date',
                        'tb_attendance_history.in_time as inTime',
                        'tb_attendance_history.out_time as outTime',
                        'tb_attendance_history.attendance_type as attendance_type'
                    )
                    ->join('tb_employee_list', 'tb_attendance_history.emp_id', 'tb_employee_list.employee_id')
                    ->where('tb_attendance_history.attendance_date', 'Like', "%$maxdate%")
                    ->where('tb_attendance_history.emp_id', '=', $employeeList->employee_id)
                    ->first();

                //Employee attendance

                //Present Status
                $status = '';
                if (in_array($today, $holidays) && !empty($attendances->inTime)) {
                    $status = 'Present-Holiday';
                }elseif (in_array($today, $holidays)) {
                    $status = 'Holiday';
                }elseif (in_array($day, $weekend) && !empty($attendances->inTime)) {
                    $status = 'Present-Weekend';
                } elseif (in_array($day, $weekend)) {
                    $status = 'Weekend';
                } elseif (!empty($attendances->inTime)) {
                    $status = 'Present';
                } else {
                    $leaveLists = '';
                    $status = 'Absent';
                    $leaveLists = DB::table('tb_employee_leave_application')
                        ->where('employee_id', '=', $employeeList->employee_primary_id)
                        ->whereYear('leave_starting_date', Carbon::now()->year)
                        ->where('status', '=', 1)->get();

                    if (!empty($leaveLists)) {
                        foreach ($leaveLists as $leaveList) {
                            if ($leaveList->leave_starting_date <= $today &&  $leaveList->leave_ending_date >= $today) {
                                $status = 'Leave';
                            } else {
                                $status = 'Absent';
                            }
                        }
                    } else {
                        $status = 'Absent';
                    }
                }


                // Present Status
                // Late Calculation
                $late = '';
                if (!empty($attendances->inTime)) {

                    $office_start_late = strtotime($office_start_time);

                    $attendance_in_time_late = strtotime($attendances->inTime);

                    if ($office_start_late < $attendance_in_time_late) {

                        $buffer_time = gmdate('H:i', strtotime($office_buffer_time) - strtotime($office_start_time));

                        $late = gmdate('H:i', $attendance_in_time_late - $office_start_late);

                        if ($late > $buffer_time) {
                            $late = $late;
                        } else {
                            $late = '';
                        }
                    }
                }

                // Late Calculation
                // Working Hour Calculation
                $work_hour = "";
                if (!empty($attendances->inTime) && !empty($attendances->outTime)) {
                    $work_hour = gmdate('H:i', strtotime($attendances->outTime) - strtotime($attendances->inTime));
                }

                //Working Hour Calculation
                //Over Time Calculation
                $over_time = '';
                $total_work_hour = '';
                if ($work_hour != '') {
                    $total_work_hour = gmdate('H:i', strtotime($office_end_time) - strtotime($office_start_time));
                    if (strtotime($work_hour) > strtotime($total_work_hour)) {
                        $over_time = gmdate('H:i', strtotime($work_hour) - strtotime($total_work_hour));
                    }
                }

                // Over Time Calculation

                // Early Deparature Calculation
                $early_deparature = '';
                if (!empty($attendances->outTime)) {
                    if ($attendances->outTime < $office_end_time) {
                        $early_deparature = gmdate('H:i', strtotime($office_end_time) - strtotime($attendances->outTime));
                    }
                }

                //Early Deparature Calculation
                if(!empty($late)){
                    $status="Late";
                }

                $attendance_data[] = [
                    'entry_date' => $attendances->date ?? date('d-M-Y', strtotime($maxdate)),
                    'day' => $day ?? '',
                    'employee_id' => $employeeList->employee_id,
                    'employee_name' => $employeeList->emp_first_name." ".$employeeList->emp_last_name,
                    'shift' => $employeeList->shift_name ?? $office_time_default->shift_name,
                    'start_time' => $office_start_time,
                    'in_time' => $attendances->inTime ?? '',
                    'attendance_type' => $attendances->attendance_type ?? '',
                    'late' => $late ?? '',
                    'end_time' => $office_end_time,
                    'out_time' => $attendances->outTime ?? '',
                    'early_deparature' => $early_deparature ?? '',
                    'total_work_hour' => $total_work_hour ?? "",
                    'work_hour' => $work_hour ?? "",
                    'over_time' => $over_time ?? "",
                    'status' => $status
                ];
                }

                $maxdate = date('Y-m-d', (strtotime('-1 day', strtotime($maxdate))));

        };

        return view("attendance_management.myself_date_wise_attendance_data", compact('attendance_data', 'request'));
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
          
        // Declare an empty array 
        $array = array(); 
          
        // Variable that store the date interval 
        // of period 1 day 
        $interval = new DateInterval('P1D'); 
      
        $realEnd = new DateTime($end); 
        $realEnd->add($interval); 
      
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
      
        // Use loop to store date into array 
        foreach($period as $date) {                  
            $array[] = $date->format($format);  
        } 
      
        // Return the array elements 
        return $array; 
    }
}
