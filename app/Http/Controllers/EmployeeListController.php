<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeListController extends Controller
{    
  public function employee_list(){

        $employee_list = DB::table('tb_employee_list')
        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
        ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_card_number', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
        ->orderBy('tb_employee_list.employee_id', 'asc')
        ->get();
        
       return view('employee_list.list', compact('employee_list'));
   } 
  
  public function employee_list_general()
  {
      $user_company = Auth::user()->company_id;

      $employee_list_general = DB::table('tb_employee_list')
      ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
      ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
      ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
      ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_blood_group', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_email', 'tb_employee_list.emp_ipbx_extension', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
      ->orderBy('tb_employee_list.employee_id', 'asc')
      ->where('tb_employee_list.company_id', '=', $user_company)
      ->get();
        
      return view('employee_list.employee_list_general', compact('employee_list_general'));
   } 


  public function employee_details($id){
    
    $id = base64_decode($id);
        $user_company = Auth::user()->company_id;

        $employee = DB::table('tb_employee_list')
        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
        ->leftjoin('users','tb_employee_list.id','=','users.ref_id')
        ->select('tb_employee_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name', 'tb_gender_list.gender_name', 'users.user_type')
        ->where('tb_employee_list.id', '=', $id)
        ->first();


      $employee_education=DB::table('tb_employee_education_info')->where('tb_employee_education_info.emp_id', '=', $id)
      ->orderBy('tb_employee_education_info.id','DESC')
      ->get();

      $employee_nominee=DB::table('tb_employee_nominee')->where('tb_employee_nominee.emp_id', '=', $id)
      ->orderBy('tb_employee_nominee.id','DESC')
      ->get();

      $working_history=DB::table('tb_employee_work_history')->where('tb_employee_work_history.emp_id', '=', $id)
      ->orderBy('tb_employee_work_history.id','DESC')
      ->get();

      $additional_information=DB::table('tb_employee_others_info')->where('tb_employee_others_info.emp_id', '=', $id)
      ->orderBy('tb_employee_others_info.id','DESC')
      ->get();

 
      $gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();
 
      $department_list=DB::table('tb_department_list')->where('tb_department_list.status', '=', 1)->orderBy('tb_department_list.id', 'asc')->get();
      
      $designation_list=DB::table('tb_designation_list')->where('tb_designation_list.status', '=', 1)->orderBy('tb_designation_list.id', 'asc')->get();

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.id', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.id', 'asc')->get();
        }
 
        // dd($employee);
       return view('employee_list.employee_details', compact('employee', 'employee_education', 'employee_nominee', 'working_history', 'additional_information', 'gender_list', 'department_list', 'designation_list', 'company_list'));
   }


  public function my_profile()
  {  
      $id = Auth::user()->ref_id;
      $user_company = Auth::user()->company_id;

      $employee = DB::table('tb_employee_list')
        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
        ->leftjoin('users','tb_employee_list.id','=','users.ref_id')
        ->select('tb_employee_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name', 'tb_gender_list.gender_name', 'users.user_type')
        ->where('tb_employee_list.id', '=', $id)
        ->first();

      $employee_education=DB::table('tb_employee_education_info')->where('tb_employee_education_info.emp_id', '=', $id)
      ->orderBy('tb_employee_education_info.id','DESC')
      ->get();

      $employee_nominee=DB::table('tb_employee_nominee')->where('tb_employee_nominee.emp_id', '=', $id)
      ->orderBy('tb_employee_nominee.id','DESC')
      ->get();

      $working_history=DB::table('tb_employee_work_history')->where('tb_employee_work_history.emp_id', '=', $id)
      ->orderBy('tb_employee_work_history.id','DESC')
      ->get();

      $additional_information=DB::table('tb_employee_others_info')->where('tb_employee_others_info.emp_id', '=', $id)
      ->orderBy('tb_employee_others_info.id','DESC')
      ->get();
 
        // dd($employee);
       return view('employee_list.my_profile', compact('employee', 'employee_education', 'employee_nominee', 'working_history', 'additional_information'));
   }

  public function my_profile_password_update(Request $request)
  {  
    $id = Auth::user()->ref_id;
    $user_info = DB::table('users')->where('users.id', '=', $id)->first();
    if($request->security_system==$request->security_user){
      if($request->new_password==$request->confirm_new_password){
          $str = DB::table('users')->where('ref_id', '=', $id)->update([
            'password'       => bcrypt($request->new_password),
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);
        return redirect('/')->with(Auth::logout());
      }else{
        Session::flash('failedMessage','Password not matched.');
      }
    }else{
      Session::flash('failedMessage','Summation of Security CAPTCHA not matched.');
    }
    
    return redirect()->back();
  }

  public function employee_password_update(Request $request)
  {  
    $id = $request->ref_id;
    $user_info = DB::table('users')->where('users.id', '=', $id)->first();
    if($request->security_system==$request->security_user){
      if($request->new_password==$request->confirm_new_password){
          $str = DB::table('users')->where('ref_id', '=', $id)->update([
            'password'       => bcrypt($request->new_password),
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);
        Session::flash('successMessage','Password has been successfully updated.');
      }else{
        Session::flash('failedMessage','Password not matched.');
      }
    }else{
      Session::flash('failedMessage','Summation of Security CAPTCHA not matched.');
    }
    
    return redirect()->back();
  }

  public function employee_access_role_create(Request $request)
  {  

    $id = $request->ref_id;
    if($request->security_system==$request->security_user){
          $employee_info = DB::table('tb_employee_list')->where('tb_employee_list.id', '=', $id)->first();
          // dd($employee_info);
          $str = DB::table('users')->insert([
            'ref_id'         => $request->ref_id,
            'user_type'      => $request->userType,
            'company_id'     => $employee_info->company_id,
            'name'           => $employee_info->emp_first_name." ".$employee_info->emp_last_name,
            'email'          => $request->emp_email,
            'password'       => bcrypt($request->emp_password),
            'status'         => 1,
            'created_at'     => Carbon::now()->toDateTimeString(),
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);

          $str = DB::table('tb_employee_list')->where('id', '=', $id)->update([
            'emp_email'      => $request->emp_email,
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);

        Session::flash('successMessage','Access Role has been successfully created.');
    }else{
      Session::flash('failedMessage','Summation of Security CAPTCHA not matched.');
    }
    return redirect()->back();
  }

  public function employee_access_role_update(Request $request)
  {  

    $id = $request->ref_id;
    if($request->security_system==$request->security_user){
          $employee_info = DB::table('tb_employee_list')->where('tb_employee_list.id', '=', $id)->first();
          // dd($employee_info);
          $str = DB::table('users')->where('ref_id', '=', $id)->update([
            'user_type'      => $request->userType,
            'email'          => $request->emp_email,
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);

          $str = DB::table('tb_employee_list')->where('id', '=', $id)->update([
            'emp_email'      => $request->emp_email,
            'updated_at'     => Carbon::now()->toDateTimeString()
          ]);
          
        Session::flash('successMessage','Access Role Information has been successfully updated.');
    }else{
      Session::flash('failedMessage','Summation of Security CAPTCHA not matched.');
    }
    return redirect()->back();
  }

  public function new_employee()
  {
 
      $gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();
 
      $department_list=DB::table('tb_department_list')->where('tb_department_list.status', '=', 1)->orderBy('tb_department_list.department_name', 'asc')->get();
      
      $designation_list=DB::table('tb_designation_list')->where('tb_designation_list.status', '=', 1)->orderBy('tb_designation_list.designation_name', 'asc')->get();

        $user_company = Auth::user()->company_id;
        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }
       return view('employee_list.create', compact('gender_list', 'department_list', 'designation_list', 'company_list'));
    }

  public function store_new_employee(Request $request)
  {
    $employee_ref_id = DB::table('tb_employee_list')->insertGetId([
      'employee_id'         => $request->employee_id,
      'emp_first_name'      => $request->emp_first_name,
      'emp_last_name'       =>  $request->emp_last_name,
      'emp_father_name'     =>  $request->emp_father_name,
      'emp_mother_name'     =>  $request->emp_mother_name,
      'emp_email'           =>  $request->emp_email,
      'emp_phone'           =>  $request->emp_phone,
      'emp_gender_id'       =>  $request->emp_gender_id,
      'company_id'          =>  $request->emp_company_id,
      'emp_department_id'   =>  $request->emp_department_id,
      'emp_designation_id'  =>  $request->emp_designation_id,
      'emp_dob'             =>  $request->emp_dob,
      'emp_joining_date'    =>  $request->emp_joining_date,
      'emp_blood_group'     =>  $request->emp_blood_group,
      'emp_religion'        =>  $request->emp_religion,
      'emp_marital_status'  =>  $request->emp_marital_status,
      'emp_card_number'     =>  $request->emp_card_number,
      'emp_nid'             =>  $request->emp_nid,
      'emp_nationality'     =>  $request->emp_nationality,
      'emp_ipbx_extension'  =>  $request->emp_ipbx_extension,
      'emp_bank_account'    =>  $request->emp_bank_account,
      'emp_bank_info'       =>  $request->emp_bank_info,
      'emp_current_address' =>  $request->emp_current_address,
      'emp_parmanent_address'=> $request->emp_parmanent_address, 
      'emp_account_status'  => 1,
      'created_by'          => Auth::user()->id,
      'created_at'          => Carbon::now()->toDateTimeString(),
      'updated_at'          => Carbon::now()->toDateTimeString()
    ]);

    if($request->giveSoftAccessId==1){
      $str = DB::table('users')->insert([
        'ref_id'         => $employee_ref_id,
        'user_type'      => $request->userType,
        'company_id'     => $request->emp_company_id,
        'name'           => $request->emp_first_name." ".$request->emp_last_name,
        'email'          => $request->emp_email,
        'password'       => bcrypt($request->password),
        'status'         => 1,
        'created_at'     => Carbon::now()->toDateTimeString(),
        'updated_at'     => Carbon::now()->toDateTimeString()
      ]);
    }

    Session::flash('successMessage','New Employee has been successfully added.');
    return redirect()->back();
  }

  public function update_employee_information(Request $request)
  {
    $employee_ref_id = DB::table('tb_employee_list')->where('id', $request->ref_id)->update([
      'employee_id'         => $request->employee_id,
      'emp_first_name'      => $request->emp_first_name,
      'emp_last_name'       =>  $request->emp_last_name,
      'emp_father_name'     =>  $request->emp_father_name,
      'emp_mother_name'     =>  $request->emp_mother_name,
      'emp_email'           =>  $request->emp_email,
      'emp_phone'           =>  $request->emp_phone,
      'emp_gender_id'       =>  $request->emp_gender_id,
      'company_id'          =>  $request->emp_company_id,
      'emp_department_id'   =>  $request->emp_department_id,
      'emp_designation_id'  =>  $request->emp_designation_id,
      'emp_dob'             =>  $request->emp_dob,
      'emp_joining_date'    =>  $request->emp_joining_date,
      'emp_blood_group'     =>  $request->emp_blood_group,
      'emp_religion'        =>  $request->emp_religion,
      'emp_marital_status'  =>  $request->emp_marital_status,
      'emp_card_number'     =>  $request->emp_card_number,
      'emp_nid'             =>  $request->emp_nid,
      'emp_nationality'     =>  $request->emp_nationality,
      'emp_ipbx_extension'  =>  $request->emp_ipbx_extension,
      'emp_bank_account'    =>  $request->emp_bank_account,
      'emp_bank_info'       =>  $request->emp_bank_info,
      'emp_current_address' =>  $request->emp_current_address,
      'emp_parmanent_address'=> $request->emp_parmanent_address, 
      'emp_account_status'  =>  $request->emp_account_status,
      'created_by'          => Auth::user()->id,
      'updated_at'          => Carbon::now()->toDateTimeString()
    ]);

    Session::flash('successMessage','Employee information has been successfully updated.');
    return redirect()->back();
  }

  //employee profile Photo update
  public function employee_profile_photo(Request $request){

        $validatedData = $request->validate([
          'emp_photo' => 'required',
        ]);
      $id = base64_decode($request->id);
      $photoName='';
      if ($request->hasFile('emp_photo')) {
          $file=DB::table('tb_employee_list')->where('id',$id)->select('emp_photo')->first();
          if(!empty($file->emp_photo)){
            if(($file->emp_photo != "female.png")||($file->emp_photo != "male.png")||($file->emp_photo != "default.png")){
              $file_path="employee_profile_image"."/".$file->emp_photo;
              if(file_exists($file_path)){
                unlink($file_path);
              }
            }
          }

          $photoName = time().'.'.$request->emp_photo->getClientOriginalExtension();
          $request->emp_photo->move('employee_profile_image', $photoName);
      
          $photo=DB::table('tb_employee_list')->where('id',$id)->update([
              'emp_photo' =>$photoName,
              'created_by'  =>auth()->user()->id,
              'updated_at'=>Carbon::now()->toDateTimeString()
          ]);
      }

        Session::flash('successMessage','Profile photo has been successfully updated');
        return redirect()->back();
  }

}
