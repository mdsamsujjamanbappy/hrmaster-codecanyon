<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NoticeListController extends Controller
{
    
  	public function new_notice(){
       return view('notice_list.add_new_notice');
   	} 

	public function store(Request $request)
    {   
	     $attachment='';
	     if ($request->hasFile('attachment')) {
	         $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
	         $request->attachment->move('notice_attachment', $attachment);
	     }

        $str = DB::table('tb_notice_list')->insert([
            'notice_title'	=>	$request->notice_title,
            'start_date'	=>	$request->start_date,
            'end_date'		=>	$request->end_date,
            'description'	=>	$request->description,
            'attachment'	=>	$request->attachment,
            'status'		=>	1,
      		'created_by'    => 	Auth::user()->id,
            'created_at'	=>	Carbon::now()->toDateTimeString(),
            'updated_at'	=>	Carbon::now()->toDateTimeString()
        ]);

    	Session::flash('successMessage','New notice information has been successfully saved.');
        return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
    	DB::table('tb_notice_list')->where('id', '=', $id)->delete();
    	Session::flash('successMessage','Notice information has been successfully destroyed.');
        return redirect()->back();
    }


	public function notice_list()
    {   
    	$notice_list = DB::table('tb_notice_list')
        ->leftJoin('users','tb_notice_list.created_by','=','users.id')
        ->select('tb_notice_list.*', 'users.name')
        ->get();
        // dd($notice_list);
        return view('notice_list.notice_list', compact('notice_list'));
    }

	public function notice_list_general()
    {   
    	$notice_list = DB::table('tb_notice_list')
        ->leftJoin('users','tb_notice_list.created_by','=','users.id')
        ->select('tb_notice_list.*', 'users.name')
        ->get();
        // dd($notice_list);
        return view('notice_list.notice_list_general', compact('notice_list'));
    }

}
