<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeWorkShiftController extends Controller
{
    public function work_shift_list()
    {   
        $work_shift_list = DB::table('tb_work_shift_list')->get();
        $user_company = Auth::user()->company_id;
        if(!empty($user_company)){
            $weekend_holiday = DB::table('tb_weekend_holiday')->where('company_id', '=', $user_company)->get();
        }else{
            $weekend_holiday = DB::table('tb_weekend_holiday')->where('company_id', '=', 1)->get();
        }

        return view('work_shift_list.work_shift_list', compact('work_shift_list', 'weekend_holiday'));
    }

    public function store(Request $request)
    {
        $str = DB::table('tb_work_shift_list')->insertGetId([
            'shift_name'    =>  $request->shift_name,
            'entry_time'    =>  $request->entry_time,
            'exit_time'     =>  $request->exit_time,
            'buffer_time'   =>  $request->buffer_time,
            'remarks'       =>  $request->remarks,
            'status'        =>  $request->status,
            'created_by'    =>  Auth::user()->id,
            'created_at'    =>  Carbon::now()->toDateTimeString(),
            'updated_at'    =>  Carbon::now()->toDateTimeString()
        ]);

        if(!empty($request->weekends)){
            foreach($request->weekends as $weekends){
              DB::table('tb_employee_shift_weekend')->insertGetId([
                    'shift_id'      =>  $str,
                    'day_name'      =>  $weekends,
                    'status'        =>  1,
                    'created_by'    =>  Auth::user()->id,
                    'created_at'    =>  Carbon::now()->toDateTimeString(),
                    'updated_at'    =>  Carbon::now()->toDateTimeString()
                ]);
            }
        }

        Session::flash('successMessage','New work shift has been successfully added.');
        return redirect()->back();
    }

    public function weekend_update(Request $request)
    {
        if(!empty($request->weekends)){
            DB::table('tb_employee_shift_weekend')->where('shift_id', '=', $request->id)->delete();
            foreach($request->weekends as $weekends){
              DB::table('tb_employee_shift_weekend')->insert([
                    'shift_id'      =>  $request->id,
                    'day_name'      =>  $weekends,
                    'status'        =>  1,
                    'created_by'    =>  Auth::user()->id,
                    'created_at'    =>  Carbon::now()->toDateTimeString(),
                    'updated_at'    =>  Carbon::now()->toDateTimeString()
                ]);
            }
        }

        Session::flash('successMessage','Weekend has been successfully updated.');
        return redirect()->back();
    }

    public function edit($id)
    {   
        $id=base64_decode($id);
        $work_shift_list = DB::table('tb_work_shift_list')->where('id', '=', $id)->first();
        return response()->json($work_shift_list);
    }

    public function weekend_edit($id)
    {   
        $id=base64_decode($id);
        $data1 = array();
        $data = DB::table('tb_employee_shift_weekend')->where('shift_id', '=', $id)->get();
        $data1['shift_id']= $id;
        foreach($data as $d){
            $data1[$d->day_name]=$d->day_name;
        }

        return response()->json($data1);
    }

    public function update(Request $request)
    {
        $str = DB::table('tb_work_shift_list')->where('id', '=', $request->id)->update([
            'shift_name'	=>	$request->shift_name,
            'entry_time'	=>	$request->entry_time,
            'exit_time'		=>	$request->exit_time,
            'buffer_time'   =>  $request->buffer_time,
            'remarks'		=>	$request->remarks,
            'status'		=>	$request->status,
      		'created_by'    => 	Auth::user()->id,
            'updated_at'	=>	Carbon::now()->toDateTimeString()
        ]);

	    Session::flash('successMessage','Work shift information has been successfully updated.');
	    return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        $emp_shift_count = DB::table('tb_employee_list')->where('emp_shift_id', '=', $id)->count();

        if($emp_shift_count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	$work_shift_list = DB::table('tb_work_shift_list')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Work shift has been successfully destroyed.');
        }

        return redirect()->back();
    }
}