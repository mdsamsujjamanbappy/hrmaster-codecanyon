<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use App\Mail\WelcomeMail;
use App\Jobs\SendEmailTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function Dashboard(){

        $employee_count = DB::table('tb_employee_list')
        ->where('tb_employee_list.emp_account_status', '=', 1)
        ->count();

        $company_count = DB::table('tb_company_information')
        ->count();

        $department_count = DB::table('tb_department_list')
        ->count();

        $designation_count = DB::table('tb_designation_list')
        ->count();

        $leave_type_count = DB::table('tb_employee_leave_type_setting')
        ->count();

        $expense_category_count = DB::table('tb_expense_category')
        ->count();

        $work_shift_count = DB::table('tb_work_shift_list')
        ->count();

        $employee_award_count = DB::table('tb_employee_award_history')
        ->count();

        $notice_list_count = DB::table('tb_notice_list')
        ->count();

        $pending_leave_count = DB::table('tb_employee_leave_application')
        ->where('tb_employee_leave_application.status', '=', 0)
        ->count();

        $approved_leave_count = DB::table('tb_employee_leave_application')
        ->where('tb_employee_leave_application.status', '=', 1)
        ->count();

        $today = date('Y-m-d');
        $todays_expense = DB::table('tb_expense_list')
        ->whereDate('tb_expense_list.expense_date', '=', $today)
        ->sum('amount');

        return view('dashboard.dashboard', compact('employee_count', 'company_count', 'department_count', 'designation_count', 'leave_type_count', 'expense_category_count', 'work_shift_count', 'todays_expense', 'employee_award_count', 'notice_list_count', 'pending_leave_count', 'approved_leave_count'));
    }

    public function mail(){
    	$details['email'] = 'system@dongyi-bd.com';

	    dispatch(new SendEmailTest($details));

	    dd('done');
    }
    
    public function message(){
        return view('message');
        
    }
}
