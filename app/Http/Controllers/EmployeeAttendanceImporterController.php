<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use App\Imports\AttendanceExcelImport;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeAttendanceImporterController extends Controller
{
    public function import_from_excel()
    {
        return view("attendance_management.importer.from_excel_view");
    }
        
    public function import_from_excel_store(Request $request)
    {
    	Excel::import(new AttendanceExcelImport, request()->file('excel_file'));

        $att=DB::select("SELECT tb_attendance_history_tmp.emp_id, MIN(TIME(tb_attendance_history_tmp.punch_time)) as in_time, MAX(TIME(tb_attendance_history_tmp.punch_time)) as out_time, DATE(tb_attendance_history_tmp.attendance_date) as date FROM tb_attendance_history_tmp JOIN tb_employee_list ON tb_employee_list.employee_id=tb_attendance_history_tmp.emp_id GROUP BY tb_attendance_history_tmp.emp_id, DATE(tb_attendance_history_tmp.attendance_date) HAVING tb_attendance_history_tmp.emp_id is not null");

        $ids=[];
        foreach ($att as $key => $value) {
            $emp_id=$value->emp_id;
            if($emp_id!=null){
                $checkEmp = DB::table('tb_attendance_history')->where(['emp_id' => $emp_id, 'attendance_date' => $value->date])->get();
                if(count($checkEmp)){
                    $id = $checkEmp[0]->id;
                    $ids[]=$id;
                    $ddate=$value->date;
                    $otime=$value->out_time;
                    if($checkEmp[0]->in_time>$otime)
                    {
                        $temp=$checkEmp[0]->in_time;
                        $checkEmp[0]->in_time=$otime;
                        $otime=$temp;
                    }
                    $updateInsert[]=[
                        'id'        =>  $id,
                        'emp_id'    =>  $emp_id,
                        'in_time'   =>  $checkEmp[0]->in_time,
                        'out_time'  =>  $otime,
                        'attendance_date'   =>  $ddate,
                        'attendance_type' => 1,
                    ];
                }
                else{
                    $secondInsert[] = [
                        'emp_id'    => $emp_id,
                        'in_time'   => $value->in_time,
                        'out_time'  => $value->out_time,
                        'attendance_date' => $value->date,
                        'attendance_type' => 1,
                    ];
                }
            }
        }

        if(!empty($ids))
        {
            DB::table('tb_attendance_history')->whereIn('id', $ids)->delete();
            DB::table('tb_attendance_history')->insert($updateInsert);
        }

        if (!empty($secondInsert)) {
            DB::table('tb_attendance_history')->insert($secondInsert);
        }

        DB::table('tb_attendance_history_tmp')->truncate();

        // dd($att);

        Session::flash('successMessage','Attendance data has been successfully imported.');
        return redirect()->back();
    }

    public function test(){
    	return date('h:i a', strtotime("2/17/2020 5:34:55 AM"));
    }

}
