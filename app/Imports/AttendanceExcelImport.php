<?php

namespace App\Imports;

use App\AttendanceHistoryModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AttendanceExcelImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        return new AttendanceHistoryModel([
           'emp_id'           =>  $row['id_number'],
           'attendance_date'  =>  date('Y-m-d', strtotime($row['datetime'])),
           'punch_time'       =>  date('H:i', strtotime($row['datetime'])),
        ]);

    }

    public function headingRow(): int
    {
        return 1;
    }
}
